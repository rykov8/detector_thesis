'''Some utils for yolo net
'''
import cv2
import keras
from keras.applications.inception_v3 import InceptionV3
# from keras.applications.inception_v3 import preprocess_input
from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
import matplotlib.pyplot as plt
import numpy as np
import pickle
from random import shuffle
from sklearn.cross_validation import train_test_split
import tensorflow as tf

from resnet50 import preprocess_input
from resnet50 import ResNet50

Sx = 16
Sy = 9
Np = 13

def iou(gt_bb, pred_bb):
    """Compute intersection over union for bounding boxes.
    Args:
        gt_bb: Ground truth bounding boxes, tensor of shape (?, Sx*Sy, 4),
            pred_bb[i, :] = [x, y, sqrtw, sqrth],
            x, y in absolute coords in [0, 1], center of bb
        pred_bb: Predicted bounding boxes, tensor of shape (?, Sx*Sy, 4),
            pred_bb[i, :] = [x, y, sqrtw, sqrth],
            x, y in absolute coords in [0, 1], center of bb
    Returns:
        iou: Intersection over union for every box, tensor of shape (?, Sx*Sy).
    """
    # convert to usual coords
    pred_wh = pred_bb[:, :, 2:4]**2
    pred_upleft = pred_bb[:, :, :2] - pred_wh / 2
    pred_botright = pred_bb[:, :, :2] + pred_wh / 2

    gt_wh = gt_bb[:, :, 2:4]**2
    gt_upleft = gt_bb[:, :, :2] - gt_wh / 2
    gt_botright = gt_bb[:, :, :2] + gt_wh / 2

    # compute intersection
    inter_upleft = tf.maximum(pred_upleft, gt_upleft)
    inter_botright = tf.minimum(pred_botright, gt_botright)
    inter_wh = inter_botright - inter_upleft
    inter_wh = tf.maximum(inter_wh, 0)
    inter = inter_wh[:, :, 0] * inter_wh[:, :, 1]

    # compute union
    area_pred = pred_wh[:, :, 0] * pred_wh[:, :, 1]
    area_gt = gt_wh[:, :, 0] * gt_wh[:, :, 1]
    union = area_pred + area_gt - inter

    # compute iou and clip invalid values
    iou = tf.select(tf.less_equal(union, 0.0),
                    tf.zeros_like(union),
                    inter / union)
    return iou

# constants for loss
l_coord = 5.0
l_obj = 1.0
l_noobj = 0.5
l_probs = 1.0

def yolo_loss(y_true, y_pred_0):
    """Compute loss described in YOLO paper.
    Args:
        y_true: True values, tensor of shape (?, Sx*Sy, Np),
            y_true[k, i, :] = [x1, y1, sqrtw1, sqrth1, c1,
                               x1, y1, sqrtw1, sqrth1, c1,
                               p1, p2, p3],
            x, y in cell coordinates in [0, 1], center of bb
        y_pred: Predicted values, tensor of shape (?, Sx*Sy, Np),
            y_pred[k, i, :] = [x1, y1, sqrtw1, sqrth1, c1,
                               x2, y2, sqrtw2, sqrth2, c2,
                               p1, p2, p3],
            x, y in cell coordinates in [0, 1], center of bb
    Returns:
        loss: Loss for prediction, scalar.
    """
    # convert predicted and gt coordinates into absolute
    # TODO some refactoring here
    #here
    y_pred = tf.unpack(y_pred_0)
    print (y_pred.get_shape())
    
    xx, yy = np.meshgrid(np.arange(Sx), np.arange(Sy))
    abs_addition = np.zeros((Sx * Sy, Np))
    abs_addition[:, 0] = xx.reshape(-1)
    abs_addition[:, 5] = xx.reshape(-1)
    abs_addition[:, 1] = yy.reshape(-1)
    abs_addition[:, 6] = yy.reshape(-1)
    normalizer = np.ones_like(abs_addition)
    normalizer[:, 0] /= Sx
    normalizer[:, 5] /= Sx
    normalizer[:, 1] /= Sy
    normalizer[:, 6] /= Sy
    y_true_abs = y_true
    y_true_abs += abs_addition
    y_true_abs *= normalizer
    y_pred_abs = y_pred
    y_pred_abs += abs_addition
    y_pred_abs *= normalizer

    # compute iou for both predictors
    # iou must be considered as constant, should not influence the gradient
    a_iou = iou(y_true_abs[:, :, :4], y_pred_abs[:, :, :4])
    a_iou = tf.stop_gradient(a_iou)
    b_iou = iou(y_true_abs[:, :, 5:9], y_pred_abs[:, :, 5:9])
    b_iou = tf.stop_gradient(b_iou)

    # compute mse for predictions (both x, y and w, h)
    a_mse = tf.reduce_sum((y_true[:, :, :4] - y_pred[:, :, :4])**2, -1)
    b_mse = tf.reduce_sum((y_true[:, :, 5:9] - y_pred[:, :, 5:9])**2, -1)

    # compute masks for better predictor
    # if both iou are 0 blame pred with smaller mse
    # mask must be considered as constant, should not influence the gradient
    mask_pred = tf.to_float(tf.greater(a_iou, b_iou))
    mask_zero_iou = tf.to_float(tf.logical_and(tf.equal(a_iou, 0.0),
                                               tf.equal(b_iou, 0.0)))
    mask_mse = tf.to_float(tf.less(a_mse, b_mse))
    mask = (1 - mask_zero_iou) * mask_pred + mask_zero_iou * mask_mse
    mask = tf.stop_gradient(mask)

    # compute confidence loss
    # if there is object
    closs_obj = mask * (a_iou - y_pred[:, :, 4])**2
    closs_obj += (1 - mask) * (b_iou - y_pred[:, :, 9])**2
    closs_obj *= l_obj
    # if there is no object
    closs_noobj = mask * y_pred[:, :, 4]**2
    closs_noobj += (1 - mask) * y_pred[:, :, 9]**2
    closs_noobj *= l_noobj
    # combine
    closs = y_true[:, :, 4] * closs_obj + (1 - y_true[:, :, 4]) * closs_noobj

    # compute probability loss
    ploss = tf.reduce_sum((y_true[:, :, 10:] - y_pred[:, :, 10:])**2, -1)
    ploss *= y_true[:, :, 4]
    ploss *= l_probs

    # compute loss
    loss = mask * a_mse + (1 - mask) * b_mse
    loss *= l_coord
    loss *= y_true[:, :, 4]
    loss += closs
    loss += ploss
    loss = tf.reduce_sum(loss, -1)
    return loss

def l1_smooth(y_true, y_pred):
    """Compute L1-smooth loss as in https://arxiv.org/abs/1504.08083.
    Args:
        y_true: Ground truth bounding boxes, tensor of shape (?, Sx*Sy, 4),
            y_true[i, :] = [x, y, sqrtw, sqrth]
        y_pred: Predicted bounding boxes, tensor of shape (?, Sx*Sy, 4),
            y_pred[i, :] = [x, y, sqrtw, sqrth]
    Returns:
        l1_loss: L1-smooth loss, tensor of shape (?, Sx*Sy).
    """
    abs_loss = tf.abs(y_true - y_pred)
    sq_loss = 0.5 * (y_true - y_pred)**2
    l1_loss = tf.select(tf.less(abs_loss, 1.0), sq_loss, abs_loss - 0.5)
    return tf.reduce_sum(l1_loss, -1)


def softmax_loss(y_true, y_pred):
    """Compute L1-smooth loss as in https://arxiv.org/abs/1504.08083.
    Args:
        y_true: Ground truth bounding boxes, tensor of shape (?, Sx*Sy, 3),
            y_true[i, :] = [p1, p2, p3]
        y_pred: Predicted bounding boxes, tensor of shape (?, Sx*Sy, 3),
            y_pred[i, :] = [p1, p2, p3]
    Returns:
        softmax_loss: L1-smooth loss, tensor of shape (?, Sx*Sy).
    """
    softmax_loss = tf.nn.softmax_cross_entropy_with_logits(y_pred, y_true)
    return softmax_loss


# constants for loss
l_coord = 5.0
l_noobj = 0.5
l_softmax = 1.0

def yolo_loss_2(y_true, y_pred):
    """Compute loss described in YOLO paper.
    Args:
        y_true: True values, tensor of shape (?, Sx*Sy, Np),
            y_true[k, i, :] = [x1, y1, sqrtw1, sqrth1, c1,
                               x1, y1, sqrtw1, sqrth1, c1,
                               p1, p2, p3],
            x, y in cell coordinates in [0, 1], center of bb
        y_pred: Predicted values, tensor of shape (?, Sx*Sy, Np),
            y_pred[k, i, :] = [x1, y1, sqrtw1, sqrth1, c1,
                               x2, y2, sqrtw2, sqrth2, c2,
                               p1, p2, p3],
            x, y in cell coordinates in [0, 1], center of bb
    Returns:
        loss: Loss for prediction, tensor of shape (?,).
    """
    # convert predicted and gt coordinates into absolute
    # TODO some refactoring here
    xx, yy = np.meshgrid(np.arange(Sx), np.arange(Sy))
    abs_addition = np.zeros((Sx * Sy, Np))
    abs_addition[:, 0] = xx.reshape(-1)
    abs_addition[:, 5] = xx.reshape(-1)
    abs_addition[:, 1] = yy.reshape(-1)
    abs_addition[:, 6] = yy.reshape(-1)
    normalizer = np.ones_like(abs_addition)
    normalizer[:, 0] /= Sx
    normalizer[:, 5] /= Sx
    normalizer[:, 1] /= Sy
    normalizer[:, 6] /= Sy
    y_true_abs = y_true
    y_true_abs += abs_addition
    y_true_abs *= normalizer
    y_pred_abs = y_pred
    y_pred_abs += abs_addition
    y_pred_abs *= normalizer

    # compute iou for both predictors
    # iou must be considered as constant, should not influence the gradient
    a_iou = iou(y_true_abs[:, :, :4], y_pred_abs[:, :, :4])
    a_iou = tf.stop_gradient(a_iou)
    b_iou = iou(y_true_abs[:, :, 5:9], y_pred_abs[:, :, 5:9])
    b_iou = tf.stop_gradient(b_iou)

    # compute mse for predictions (both x, y and w, h)
    a_mse = l1_smooth(y_true[:, :, :4], y_pred[:, :, :4])
    b_mse = l1_smooth(y_true[:, :, 5:9], y_pred[:, :, 5:9])

    # compute masks for better predictor
    # if both iou are 0 blame pred with smaller mse
    # mask must be considered as constant, should not influence the gradient
    mask_pred = tf.to_float(tf.greater(a_iou, b_iou))
    mask_zero_iou = tf.to_float(tf.logical_and(tf.equal(a_iou, 0.0),
                                               tf.equal(b_iou, 0.0)))
    mask_mse = tf.to_float(tf.less(a_mse, b_mse))
    mask = (1 - mask_zero_iou) * mask_pred + mask_zero_iou * mask_mse
    mask = tf.stop_gradient(mask)

    # compute confidence loss
    # if there is object
    closs_obj = mask * (a_iou - y_pred[:, :, 4])**2
    closs_obj += (1 - mask) * (b_iou - y_pred[:, :, 9])**2
    # if there is no object
    closs_noobj = mask * y_pred[:, :, 4]**2
    closs_noobj += (1 - mask) * y_pred[:, :, 9]**2
    closs_noobj *= l_noobj
    # combine
    closs = y_true[:, :, 4] * closs_obj + (1 - y_true[:, :, 4]) * closs_noobj

    # compute probability loss
    ploss = softmax_loss(y_true[:, :, 10:], y_pred[:, :, 10:])
    ploss *= y_true[:, :, 4]
    ploss *= l_softmax

    # compute loss
    loss = mask * a_mse + (1 - mask) * b_mse
    loss *= l_coord
    loss *= y_true[:, :, 4]
    loss += closs
    loss += ploss
    loss = tf.reduce_sum(loss, -1)
    return loss

class_dict = ['bottle', 'white bottle', 'can']

def numpy_iou(box1,box2):
    tb = min(box1[0]+0.5*box1[2],box2[0]+0.5*box2[2])-max(box1[0]-0.5*box1[2],box2[0]-0.5*box2[2])
    lr = min(box1[1]+0.5*box1[3],box2[1]+0.5*box2[3])-max(box1[1]-0.5*box1[3],box2[1]-0.5*box2[3])
    if tb < 0 or lr < 0 : intersection = 0
    else : intersection =  tb*lr
    return intersection / (box1[2]*box1[3] + box2[2]*box2[3] - intersection)


def internpret_prediction(pred, img, threshold=0.35, iou_threshold=0.4):
    xx, yy = np.meshgrid(np.arange(Sx), np.arange(Sy))
    abs_addition = np.zeros((Sx * Sy, Np))
    abs_addition[:, 0] = xx.reshape(-1)
    abs_addition[:, 5] = xx.reshape(-1)
    abs_addition[:, 1] = yy.reshape(-1)
    abs_addition[:, 6] = yy.reshape(-1)
    normalizer = np.ones_like(abs_addition)
    normalizer[:, 0] /= Sx
    normalizer[:, 5] /= Sx
    normalizer[:, 1] /= Sy
    normalizer[:, 6] /= Sy
    pred += abs_addition
    pred *= normalizer

    probs1 = pred[:, 4][:, None] * pred[:, 10:]
    probs2 = pred[:, 9][:, None] * pred[:, 10:]
    boxes_mask = (probs1 > probs2).astype('float64')
    probs = boxes_mask * probs1 + (1 - boxes_mask) * probs2
    classes = probs.argmax(axis=-1)
    class_probs = probs.max(axis=-1)
    probs_max_mask = np.zeros_like(probs)
    probs_max_mask[np.arange(len(probs)), classes] = 1
    boxes_mask *= probs_max_mask
    boxes_mask = boxes_mask.sum(axis=-1, keepdims=True)
    boxes = boxes_mask * pred[:, :4] + (1 - boxes_mask) * pred[:, 5:9]

    argsort = np.argsort(class_probs)[::-1]
    class_probs = class_probs[argsort]
    boxes = boxes[argsort]
    classes = classes[argsort]
    probs_thresh_mask = class_probs > threshold
    class_probs = class_probs[probs_thresh_mask]
    boxes = boxes[probs_thresh_mask]
    classes = classes[probs_thresh_mask]

    for i, prob in enumerate(class_probs):
        if prob == 0: continue
        for j in range(i + 1, len(class_probs)):
            if numpy_iou(boxes[i], boxes[j]) > iou_threshold :
                class_probs[j] = 0

    final_filter = class_probs > 0
    class_probs = class_probs[final_filter]
    boxes = boxes[final_filter]
    classes = classes[final_filter]

    for i, box in enumerate(boxes):
        upleft = box[:2].copy()
        upleft[0] -= box[2]**2 / 2
        upleft[1] -= box[3]**2 / 2

        botright = box[:2].copy()
        botright[0] += box[2]**2 / 2
        botright[1] += box[3]**2 / 2

        pt1 = (int(upleft[0]*640), int(upleft[1]*360))
        pt2 = (int(botright[0]*640), int(botright[1]*360))
        color = [0, 0, 0]
        color[classes[i]] = 255
        cv2.rectangle(img, pt1, pt2, color, 2)
        cv2.putText(img, class_dict[classes[i]] + ', {:.2f}'.format(class_probs[i]),
                    (pt1[0] + 5, pt1[1] - 7), cv2.FONT_HERSHEY_SIMPLEX, 0.6, color, 2)
    return img
