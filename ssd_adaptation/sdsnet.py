"""Keras implementation of SSD with gradient reversal layer."""

import keras.backend as K
from keras.layers import Activation
from keras.layers import AtrousConvolution2D
from keras.layers import BatchNormalization
from keras.layers import Convolution2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import GlobalAveragePooling2D
from keras.layers import Input
from keras.layers import MaxPooling2D
from keras.layers import merge
from keras.layers import Reshape
from keras.layers import UpSampling2D
from keras.layers import ZeroPadding2D
from keras.models import Model

from gradient_reversal_layer import GradientReversalLayer
from ssd_layers import PriorBox


def SDSnet(input_shape, num_classes=21, dann=False):
    """SSD300 with gradient reversal layer architecture.

    # Arguments
        input_shape: Shape of the input image,
            expected to be either (300, 300, 3) or (3, 300, 300)(not tested).
        num_classes: Number of classes including background.
        dann: Whether to add adaptation classifier.

    # References
        https://arxiv.org/abs/1512.02325
        https://arxiv.org/abs/1505.07818
    """
    if K.image_dim_ordering() == 'tf':
        bn_axis = 3
    else:
        bn_axis = 1
    net = {}
    # Block 1
    input_tensor = Input(shape=input_shape, name='input')
    img_size = (input_shape[1], input_shape[0])
    net['input'] = input_tensor
    net['conv1_1'] = Convolution2D(64, 3, 3,
                                   border_mode='same',
                                   name='conv1_1')(net['input'])
    # net['conv1_1'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv1_1')(net['conv1_1'])
    net['conv1_1'] = Activation('relu')(net['conv1_1'])
    net['conv1_2'] = Convolution2D(64, 3, 3,
                                   border_mode='same',
                                   name='conv1_2')(net['conv1_1'])
    # net['conv1_2'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv1_2')(net['conv1_2'])
    net['conv1_2'] = Activation('relu')(net['conv1_2'])
    net['pool1'] = MaxPooling2D((2, 2), strides=(2, 2), border_mode='same',
                                name='pool1')(net['conv1_2'])
    # Block 2
    net['conv2_1'] = Convolution2D(128, 3, 3,
                                   border_mode='same',
                                   name='conv2_1')(net['pool1'])
    # net['conv2_1'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv2_1')(net['conv2_1'])
    net['conv2_1'] = Activation('relu')(net['conv2_1'])
    net['conv2_2'] = Convolution2D(128, 3, 3,
                                   border_mode='same',
                                   name='conv2_2')(net['conv2_1'])
    # net['conv2_2'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv2_2')(net['conv2_2'])
    net['conv2_2'] = Activation('relu')(net['conv2_2'])
    net['pool2'] = MaxPooling2D((2, 2), strides=(2, 2), border_mode='same',
                                name='pool2')(net['conv2_2'])
    # Block 3
    net['conv3_1'] = Convolution2D(256, 3, 3,
                                   border_mode='same',
                                   name='conv3_1')(net['pool2'])
    # net['conv3_1'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv3_1')(net['conv3_1'])
    net['conv3_1'] = Activation('relu')(net['conv3_1'])
    net['conv3_2'] = Convolution2D(256, 3, 3,
                                   border_mode='same',
                                   name='conv3_2')(net['conv3_1'])
    # net['conv3_2'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv3_2')(net['conv3_2'])
    net['conv3_2'] = Activation('relu')(net['conv3_2'])
    net['conv3_3'] = Convolution2D(256, 3, 3,
                                   border_mode='same',
                                   name='conv3_3')(net['conv3_2'])
    # net['conv3_3'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv3_3')(net['conv3_3'])
    net['conv3_3'] = Activation('relu')(net['conv3_3'])
    net['pool3'] = MaxPooling2D((2, 2), strides=(2, 2), border_mode='same',
                                name='pool3')(net['conv3_3'])
    # Block 4
    net['conv4_1'] = Convolution2D(512, 3, 3,
                                   border_mode='same',
                                   name='conv4_1')(net['pool3'])
    # net['conv4_1'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv4_1')(net['conv4_1'])
    net['conv4_1'] = Activation('relu')(net['conv4_1'])
    net['conv4_2'] = Convolution2D(512, 3, 3,
                                   border_mode='same',
                                   name='conv4_2')(net['conv4_1'])
    # net['conv4_2'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv4_2')(net['conv4_2'])
    net['conv4_2'] = Activation('relu')(net['conv4_2'])
    net['conv4_3'] = Convolution2D(512, 3, 3,
                                   border_mode='same',
                                   name='conv4_3')(net['conv4_2'])
    net['conv4_3'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv4_3')(net['conv4_3'])
    net['conv4_3'] = Activation('relu')(net['conv4_3'])
    net['pool4'] = MaxPooling2D((2, 2), strides=(2, 2), border_mode='same',
                                name='pool4')(net['conv4_3'])
    # Block 5
    net['conv5_1'] = Convolution2D(512, 3, 3,
                                   border_mode='same',
                                   name='conv5_1')(net['pool4'])
    net['conv5_1'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv5_1')(net['conv5_1'])
    net['conv5_1'] = Activation('relu')(net['conv5_1'])
    net['conv5_2'] = Convolution2D(512, 3, 3,
                                   border_mode='same',
                                   name='conv5_2')(net['conv5_1'])
    net['conv5_2'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv5_2')(net['conv5_2'])
    net['conv5_2'] = Activation('relu')(net['conv5_2'])
    net['conv5_3'] = Convolution2D(512, 3, 3,
                                   border_mode='same',
                                   name='conv5_3')(net['conv5_2'])
    net['conv5_3'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv5_3')(net['conv5_3'])
    net['conv5_3'] = Activation('relu')(net['conv5_3'])
    net['pool5'] = MaxPooling2D((3, 3), strides=(1, 1), border_mode='same',
                                name='pool5')(net['conv5_3'])
    # FC6
    net['fc6'] = AtrousConvolution2D(1024, 3, 3, atrous_rate=(6, 6),
                                     border_mode='same',
                                     name='fc6')(net['pool5'])
    net['fc6'] = BatchNormalization(axis=bn_axis,
                                    name='bn_fc6')(net['fc6'])
    net['fc6'] = Activation('relu')(net['fc6'])
    # FC7
    net['fc7'] = Convolution2D(1024, 1, 1,
                               border_mode='same', name='fc7')(net['fc6'])
    net['fc7'] = BatchNormalization(axis=bn_axis,
                                    name='bn_fc7')(net['fc7'])
    net['fc7'] = Activation('relu')(net['fc7'])
    # Block 6
    net['conv6_1'] = Convolution2D(256, 1, 1,
                                   border_mode='same',
                                   name='conv6_1')(net['fc7'])
    net['conv6_1'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv6_1')(net['conv6_1'])
    net['conv6_1'] = Activation('relu')(net['conv6_1'])
    net['conv6_2'] = Convolution2D(512, 3, 3, subsample=(2, 2),
                                   border_mode='same',
                                   name='conv6_2')(net['conv6_1'])
    net['conv6_2'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv6_2')(net['conv6_2'])
    net['conv6_2'] = Activation('relu')(net['conv6_2'])
    # Block 7
    net['conv7_1'] = Convolution2D(128, 1, 1,
                                   border_mode='same',
                                   name='conv7_1')(net['conv6_2'])
    net['conv7_1'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv7_1')(net['conv7_1'])
    net['conv7_1'] = Activation('relu')(net['conv7_1'])
    net['conv7_2'] = ZeroPadding2D()(net['conv7_1'])
    net['conv7_2'] = Convolution2D(256, 3, 3, subsample=(2, 2),
                                   border_mode='valid',
                                   name='conv7_2')(net['conv7_2'])
    net['conv7_2'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv7_2')(net['conv7_2'])
    net['conv7_2'] = Activation('relu')(net['conv7_2'])
    # Block 8
    net['conv8_1'] = Convolution2D(128, 1, 1,
                                   border_mode='same',
                                   name='conv8_1')(net['conv7_2'])
    net['conv8_1'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv8_1')(net['conv8_1'])
    net['conv8_1'] = Activation('relu')(net['conv8_1'])
    net['conv8_2'] = Convolution2D(256, 3, 3, subsample=(2, 2),
                                   border_mode='same',
                                   name='conv8_2')(net['conv8_1'])
    net['conv8_2'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv8_2')(net['conv8_2'])
    net['conv8_2'] = Activation('relu')(net['conv8_2'])
    # Last Pool
    net['pool6'] = GlobalAveragePooling2D(name='pool6')(net['conv8_2'])
    # Prediction from conv4_3
    # net['conv4_3_norm'] = Normalize(20, name='conv4_3_norm')(net['conv4_3'])
    net['conv4_3_norm'] = net['conv4_3']
    num_priors = 3
    x = Convolution2D(num_priors * 4, 3, 3, border_mode='same',
                      name='conv4_3_norm_mbox_loc')(net['conv4_3_norm'])
    net['conv4_3_norm_mbox_loc'] = x
    flatten = Flatten(name='conv4_3_norm_mbox_loc_flat')
    net['conv4_3_norm_mbox_loc_flat'] = flatten(net['conv4_3_norm_mbox_loc'])
    name = 'conv4_3_norm_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Convolution2D(num_priors * num_classes, 3, 3, border_mode='same',
                      name=name)(net['conv4_3_norm'])
    net['conv4_3_norm_mbox_conf'] = x
    flatten = Flatten(name='conv4_3_norm_mbox_conf_flat')
    net['conv4_3_norm_mbox_conf_flat'] = flatten(net['conv4_3_norm_mbox_conf'])
    priorbox = PriorBox(img_size, 30.0, aspect_ratios=[2],
                        variances=[0.1, 0.1, 0.2, 0.2],
                        name='conv4_3_norm_mbox_priorbox')
    net['conv4_3_norm_mbox_priorbox'] = priorbox(net['conv4_3_norm'])
    # Adaptation branch conv4_3
    if dann:
        grl = GradientReversalLayer(1.0, name='conv4_3_grl')
        net['conv4_3_grl'] = grl(net['pool4'])
        conv2d = Convolution2D(512, 3, 3, border_mode='same',
                               name='conv4_3_ad_1')
        net['conv4_3_ad_1'] = conv2d(net['conv4_3_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_conv4_3_ad_1')
        net['conv4_3_ad_1'] = bn(net['conv4_3_ad_1'])
        net['conv4_3_ad_1'] = Activation('relu')(net['conv4_3_ad_1'])
        gap = GlobalAveragePooling2D(name='conv4_3_ad_pool')
        net['conv4_3_ad_pool'] = gap(net['conv4_3_ad_1'])
    # Prediction from fc7
    num_priors = 6
    net['fc7_mbox_loc'] = Convolution2D(num_priors * 4, 3, 3,
                                        border_mode='same',
                                        name='fc7_mbox_loc')(net['fc7'])
    flatten = Flatten(name='fc7_mbox_loc_flat')
    net['fc7_mbox_loc_flat'] = flatten(net['fc7_mbox_loc'])
    name = 'fc7_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    net['fc7_mbox_conf'] = Convolution2D(num_priors * num_classes, 3, 3,
                                         border_mode='same',
                                         name=name)(net['fc7'])
    flatten = Flatten(name='fc7_mbox_conf_flat')
    net['fc7_mbox_conf_flat'] = flatten(net['fc7_mbox_conf'])
    priorbox = PriorBox(img_size, 60.0, max_size=114.0, aspect_ratios=[2, 3],
                        variances=[0.1, 0.1, 0.2, 0.2],
                        name='fc7_mbox_priorbox')
    net['fc7_mbox_priorbox'] = priorbox(net['fc7'])
    # Adaptation branch fc7
    if dann:
        grl = GradientReversalLayer(1.0, name='fc7_grl')
        net['fc7_grl'] = grl(net['fc7'])
        conv2d = Convolution2D(1024, 3, 3, border_mode='same',
                               name='fc7_ad_1')
        net['fc7_ad_1'] = conv2d(net['fc7_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_fc7_ad_1')
        net['fc7_ad_1'] = bn(net['fc7_ad_1'])
        net['fc7_ad_1'] = Activation('relu')(net['fc7_ad_1'])
        gap = GlobalAveragePooling2D(name='fc7_ad_pool')
        net['fc7_ad_pool'] = gap(net['fc7_ad_1'])
    # Prediction from conv6_2
    num_priors = 6
    x = Convolution2D(num_priors * 4, 3, 3, border_mode='same',
                      name='conv6_2_mbox_loc')(net['conv6_2'])
    net['conv6_2_mbox_loc'] = x
    flatten = Flatten(name='conv6_2_mbox_loc_flat')
    net['conv6_2_mbox_loc_flat'] = flatten(net['conv6_2_mbox_loc'])
    name = 'conv6_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Convolution2D(num_priors * num_classes, 3, 3, border_mode='same',
                      name=name)(net['conv6_2'])
    net['conv6_2_mbox_conf'] = x
    flatten = Flatten(name='conv6_2_mbox_conf_flat')
    net['conv6_2_mbox_conf_flat'] = flatten(net['conv6_2_mbox_conf'])
    priorbox = PriorBox(img_size, 114.0, max_size=168.0, aspect_ratios=[2, 3],
                        variances=[0.1, 0.1, 0.2, 0.2],
                        name='conv6_2_mbox_priorbox')
    net['conv6_2_mbox_priorbox'] = priorbox(net['conv6_2'])
    # Adaptation branch conv6_2
    if dann:
        grl = GradientReversalLayer(1.0, name='conv6_2_grl')
        net['conv6_2_grl'] = grl(net['conv6_2'])
        conv2d = Convolution2D(512, 3, 3, border_mode='same',
                               name='conv6_2_ad_1')
        net['conv6_2_ad_1'] = conv2d(net['conv6_2_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_conv6_2_ad_1')
        net['conv6_2_ad_1'] = bn(net['conv6_2_ad_1'])
        net['conv6_2_ad_1'] = Activation('relu')(net['conv6_2_ad_1'])
        gap = GlobalAveragePooling2D(name='conv6_2_ad_pool')
        net['conv6_2_ad_pool'] = gap(net['conv6_2_ad_1'])
    # Prediction from conv7_2
    num_priors = 6
    x = Convolution2D(num_priors * 4, 3, 3, border_mode='same',
                      name='conv7_2_mbox_loc')(net['conv7_2'])
    net['conv7_2_mbox_loc'] = x
    flatten = Flatten(name='conv7_2_mbox_loc_flat')
    net['conv7_2_mbox_loc_flat'] = flatten(net['conv7_2_mbox_loc'])
    name = 'conv7_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Convolution2D(num_priors * num_classes, 3, 3, border_mode='same',
                      name=name)(net['conv7_2'])
    net['conv7_2_mbox_conf'] = x
    flatten = Flatten(name='conv7_2_mbox_conf_flat')
    net['conv7_2_mbox_conf_flat'] = flatten(net['conv7_2_mbox_conf'])
    priorbox = PriorBox(img_size, 168.0, max_size=222.0, aspect_ratios=[2, 3],
                        variances=[0.1, 0.1, 0.2, 0.2],
                        name='conv7_2_mbox_priorbox')
    net['conv7_2_mbox_priorbox'] = priorbox(net['conv7_2'])
    # Adaptation branch conv7_2
    if dann:
        grl = GradientReversalLayer(1.0, name='conv7_2_grl')
        net['conv7_2_grl'] = grl(net['conv7_2'])
        conv2d = Convolution2D(256, 3, 3, border_mode='same',
                               name='conv7_2_ad_1')
        net['conv7_2_ad_1'] = conv2d(net['conv7_2_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_conv7_2_ad_1')
        net['conv7_2_ad_1'] = bn(net['conv7_2_ad_1'])
        net['conv7_2_ad_1'] = Activation('relu')(net['conv7_2_ad_1'])
        gap = GlobalAveragePooling2D(name='conv7_2_ad_pool')
        net['conv7_2_ad_pool'] = gap(net['conv7_2_ad_1'])
    # Prediction from conv8_2
    num_priors = 6
    x = Convolution2D(num_priors * 4, 3, 3, border_mode='same',
                      name='conv8_2_mbox_loc')(net['conv8_2'])
    net['conv8_2_mbox_loc'] = x
    flatten = Flatten(name='conv8_2_mbox_loc_flat')
    net['conv8_2_mbox_loc_flat'] = flatten(net['conv8_2_mbox_loc'])
    name = 'conv8_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Convolution2D(num_priors * num_classes, 3, 3, border_mode='same',
                      name=name)(net['conv8_2'])
    net['conv8_2_mbox_conf'] = x
    flatten = Flatten(name='conv8_2_mbox_conf_flat')
    net['conv8_2_mbox_conf_flat'] = flatten(net['conv8_2_mbox_conf'])
    priorbox = PriorBox(img_size, 222.0, max_size=276.0, aspect_ratios=[2, 3],
                        variances=[0.1, 0.1, 0.2, 0.2],
                        name='conv8_2_mbox_priorbox')
    net['conv8_2_mbox_priorbox'] = priorbox(net['conv8_2'])
    # Adaptation branch conv8_2
    if dann:
        grl = GradientReversalLayer(1.0, name='conv8_2_grl')
        net['conv8_2_grl'] = grl(net['conv8_2'])
        conv2d = Convolution2D(256, 3, 3, border_mode='same',
                               name='conv8_2_ad_1')
        net['conv8_2_ad_1'] = conv2d(net['conv8_2_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_conv8_2_ad_1')
        net['conv8_2_ad_1'] = bn(net['conv8_2_ad_1'])
        net['conv8_2_ad_1'] = Activation('relu')(net['conv8_2_ad_1'])
        gap = GlobalAveragePooling2D(name='conv8_2_ad_pool')
        net['conv8_2_ad_pool'] = gap(net['conv8_2_ad_1'])
    # Prediction from pool6
    num_priors = 6
    x = Dense(num_priors * 4, name='pool6_mbox_loc_flat')(net['pool6'])
    net['pool6_mbox_loc_flat'] = x
    name = 'pool6_mbox_conf_flat'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Dense(num_priors * num_classes, name=name)(net['pool6'])
    net['pool6_mbox_conf_flat'] = x
    priorbox = PriorBox(img_size, 276.0, max_size=330.0, aspect_ratios=[2, 3],
                        variances=[0.1, 0.1, 0.2, 0.2],
                        name='pool6_mbox_priorbox')
    if K.image_dim_ordering() == 'tf':
        target_shape = (1, 1, 256)
    else:
        target_shape = (256, 1, 1)
    net['pool6_reshaped'] = Reshape(target_shape,
                                    name='pool6_reshaped')(net['pool6'])
    net['pool6_mbox_priorbox'] = priorbox(net['pool6_reshaped'])
    # Adaptation branch pool6
    if dann:
        grl = GradientReversalLayer(1.0, name='pool6_grl')
        net['pool6_grl'] = grl(net['pool6'])
    # Gather all predictions
    net['mbox_loc'] = merge([net['conv4_3_norm_mbox_loc_flat'],
                             net['fc7_mbox_loc_flat'],
                             net['conv6_2_mbox_loc_flat'],
                             net['conv7_2_mbox_loc_flat'],
                             net['conv8_2_mbox_loc_flat'],
                             net['pool6_mbox_loc_flat']],
                            mode='concat', concat_axis=1, name='mbox_loc')
    net['mbox_conf'] = merge([net['conv4_3_norm_mbox_conf_flat'],
                              net['fc7_mbox_conf_flat'],
                              net['conv6_2_mbox_conf_flat'],
                              net['conv7_2_mbox_conf_flat'],
                              net['conv8_2_mbox_conf_flat'],
                              net['pool6_mbox_conf_flat']],
                             mode='concat', concat_axis=1, name='mbox_conf')
    net['mbox_priorbox'] = merge([net['conv4_3_norm_mbox_priorbox'],
                                  net['fc7_mbox_priorbox'],
                                  net['conv6_2_mbox_priorbox'],
                                  net['conv7_2_mbox_priorbox'],
                                  net['conv8_2_mbox_priorbox'],
                                  net['pool6_mbox_priorbox']],
                                 mode='concat', concat_axis=1,
                                 name='mbox_priorbox')
    if hasattr(net['mbox_loc'], '_keras_shape'):
        num_boxes = net['mbox_loc']._keras_shape[-1] // 4
    elif hasattr(net['mbox_loc'], 'int_shape'):
        num_boxes = K.int_shape(net['mbox_loc'])[-1] // 4
    net['mbox_loc'] = Reshape((num_boxes, 4),
                              name='mbox_loc_final')(net['mbox_loc'])
    net['mbox_conf'] = Reshape((num_boxes, num_classes),
                               name='mbox_conf_logits')(net['mbox_conf'])
    net['mbox_conf'] = Activation('softmax',
                                  name='mbox_conf_final')(net['mbox_conf'])
    net['predictions'] = merge([net['mbox_loc'],
                                net['mbox_conf'],
                                net['mbox_priorbox']],
                               mode='concat', concat_axis=2,
                               name='predictions')
    output = [net['predictions']]
    # Adaptation branch
    if dann:
        net['ad_feature_vec'] = merge([net['conv4_3_ad_pool'],
                                       net['fc7_ad_pool'],
                                       net['conv6_2_ad_pool'],
                                       net['conv7_2_ad_pool'],
                                       net['conv8_2_ad_pool'],
                                       net['pool6_grl']],
                                      mode='concat', concat_axis=1,
                                      name='ad_feature_vec')
        dense = Dense(1, name='ad_class_logits')
        net['ad_class_logits'] = dense(net['ad_feature_vec'])
        sigmoid = Activation('sigmoid', name='ad_class')
        net['ad_class'] = sigmoid(net['ad_class_logits'])
        output.append(net['ad_class'])

    # segmentation block
    # upconv from conv7_2
    net['upconv7_2'] = UpSampling2D(name='ups_conv7_2')(net['conv7_2'])
    net['upconv7_2'] = Convolution2D(128, 3, 3, border_mode='same',
                                     name='upconv7_2_1')(net['upconv7_2'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv7_2_1')
    net['upconv7_2'] = bn(net['upconv7_2'])
    net['upconv7_2'] = Activation('relu')(net['upconv7_2'])
    net['upconv7_2'] = merge([net['upconv7_2'], net['conv6_2']],
                             mode='concat', name='upconv7_2+conv6_2')
    net['upconv7_2'] = Convolution2D(256, 3, 3, border_mode='same',
                                     name='upconv7_2_2')(net['upconv7_2'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv7_2_2')
    net['upconv7_2'] = bn(net['upconv7_2'])
    net['upconv7_2'] = Activation('relu')(net['upconv7_2'])

    # upconv from conv6_2
    net['upconv6_2'] = UpSampling2D(name='ups_conv6_2')(net['upconv7_2'])
    zp = ZeroPadding2D(padding=(0, 1, 0, 1), name='pad_upconv6_2')
    net['upconv6_2'] = zp(net['upconv6_2'])
    net['upconv6_2'] = Convolution2D(128, 3, 3, border_mode='valid',
                                     name='upconv6_2_1')(net['upconv6_2'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv6_2_1')
    net['upconv6_2'] = bn(net['upconv6_2'])
    net['upconv6_2'] = Activation('relu')(net['upconv6_2'])
    net['upconv6_2'] = merge([net['upconv6_2'], net['fc7']],
                             mode='concat', name='upconv6_2+fc7')
    net['upconv6_2'] = Convolution2D(256, 3, 3, border_mode='same',
                                     name='upconv6_2_2')(net['upconv6_2'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv6_2_2')
    net['upconv6_2'] = bn(net['upconv6_2'])
    net['upconv6_2'] = Activation('relu')(net['upconv6_2'])

    # upconv from fc7
    net['upconv_fc7'] = UpSampling2D(name='ups_fc7')(net['upconv6_2'])
    net['upconv_fc7'] = Convolution2D(128, 3, 3, border_mode='same',
                                      name='upconv_fc7_1')(net['upconv_fc7'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv_fc7_1')
    net['upconv_fc7'] = bn(net['upconv_fc7'])
    net['upconv_fc7'] = Activation('relu')(net['upconv_fc7'])
    net['upconv_fc7'] = merge([net['upconv_fc7'], net['conv4_3']],
                              mode='concat', name='upconv_fc7+conv4_3')
    net['upconv_fc7'] = Convolution2D(256, 3, 3, border_mode='same',
                                      name='upconv_fc7_2')(net['upconv_fc7'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv_fc7_2')
    net['upconv_fc7'] = bn(net['upconv_fc7'])
    net['upconv_fc7'] = Activation('relu')(net['upconv_fc7'])

    # upconv from conv4_3
    net['upconv4_3'] = UpSampling2D(name='ups_conv4_3')(net['upconv_fc7'])
    zp = ZeroPadding2D(padding=(0, 1, 0, 1), name='pad_upconv4_3')
    net['upconv4_3'] = zp(net['upconv4_3'])
    net['upconv4_3'] = Convolution2D(128, 3, 3, border_mode='valid',
                                     name='upconv4_3_1')(net['upconv4_3'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv4_3_1')
    net['upconv4_3'] = bn(net['upconv4_3'])
    net['upconv4_3'] = Activation('relu')(net['upconv4_3'])
    net['upconv4_3'] = merge([net['upconv4_3'], net['conv3_3']],
                             mode='concat', name='conv4_3+conv3_3')
    net['upconv4_3'] = Convolution2D(128, 3, 3, border_mode='same',
                                     name='upconv4_3_2')(net['upconv4_3'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv4_3_2')
    net['upconv4_3'] = bn(net['upconv4_3'])
    net['upconv4_3'] = Activation('relu')(net['upconv4_3'])

    # upconv from conv3_3
    net['upconv3_3'] = UpSampling2D(name='ups_conv3_3')(net['upconv4_3'])
    net['upconv3_3'] = Convolution2D(64, 3, 3, border_mode='same',
                                     name='upconv3_3_1')(net['upconv3_3'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv3_3_1')
    net['upconv3_3'] = bn(net['upconv3_3'])
    net['upconv3_3'] = Activation('relu')(net['upconv3_3'])
    net['upconv3_3'] = merge([net['upconv3_3'], net['conv2_2']],
                             mode='concat', name='conv3_3+conv2_2')
    net['upconv3_3'] = Convolution2D(64, 3, 3, border_mode='same',
                                     name='upconv3_3_2')(net['upconv3_3'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv3_3_2')
    net['upconv3_3'] = bn(net['upconv3_3'])
    net['upconv3_3'] = Activation('relu')(net['upconv3_3'])

    # upconv from conv2_2
    net['upconv2_2'] = UpSampling2D(name='ups_conv2_2')(net['upconv3_3'])
    net['upconv2_2'] = Convolution2D(32, 3, 3, border_mode='same',
                                     name='upconv2_2_1')(net['upconv2_2'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv2_2_1')
    net['upconv2_2'] = bn(net['upconv2_2'])
    net['upconv2_2'] = Activation('relu')(net['upconv2_2'])
    net['upconv2_2'] = merge([net['upconv2_2'], net['conv1_2']],
                             mode='concat', name='conv2_2+conv1_2')
    net['upconv2_2'] = Convolution2D(32, 3, 3, border_mode='same',
                                     name='upconv2_2_2')(net['upconv2_2'])
    bn = BatchNormalization(axis=bn_axis, name='bn_upconv2_2_2')
    net['upconv2_2'] = bn(net['upconv2_2'])
    net['upconv2_2'] = Activation('relu')(net['upconv2_2'])

    # segmentation prediction
    net['seg_logits'] = Convolution2D(num_classes, 1, 1,
                                      name='seg_logits')(net['upconv2_2'])
    net['seg_reshape'] = Reshape((-1, num_classes),
                                 name='seg_reshape')(net['seg_logits'])
    net['seg'] = Activation('softmax', name='segmentation')(net['seg_reshape'])
    output.append(net['seg'])

    model = Model(net['input'], output)
    return model
