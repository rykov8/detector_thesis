"""Some special pupropse layers for SSD."""

from keras import initializers
from keras import regularizers
import keras.backend as K
from keras.engine import InputSpec
from keras.engine import Layer
import numpy as np
import tensorflow as tf
from tensorflow.python.training.moving_averages import assign_moving_average


class Normalize(Layer):
    """Normalization layer as described in ParseNet paper.

    # Arguments
        scale: Default feature scale.

    # Input shape
        4D tensor with shape:
        `(samples, channels, rows, cols)` if dim_ordering='th'
        or 4D tensor with shape:
        `(samples, rows, cols, channels)` if dim_ordering='tf'.

    # Output shape
        Same as input

    # References
        http://cs.unc.edu/~wliu/papers/parsenet.pdf

    #TODO
        Add possibility to have one scale for all features.
    """
    def __init__(self, scale, **kwargs):
        if K.image_dim_ordering() == 'tf':
            self.axis = 3
        else:
            self.axis = 1
        self.scale = scale
        super(Normalize, self).__init__(**kwargs)

    def build(self, input_shape):
        self.input_spec = [InputSpec(shape=input_shape)]
        shape = (input_shape[self.axis],)
        init_gamma = self.scale * np.ones(shape)
        self.gamma = K.variable(init_gamma, name='{}_gamma'.format(self.name))
        self.trainable_weights = [self.gamma]
        self.built = True

    def call(self, x, mask=None):
        output = K.l2_normalize(x, self.axis)
        output *= self.gamma
        return output


class PriorBox(Layer):
    """Generate the prior boxes of designated sizes and aspect ratios.

    # Arguments
        img_size: Size of the input image as tuple (w, h).
        min_size: Minimum box size in pixels.
        max_size: Maximum box size in pixels.
        aspect_ratios: List of aspect ratios of boxes.
        flip: Whether to consider reverse aspect ratios.
        clip: Whether to clip the prior's coordinates
            such that they are within [0, 1].
        variances: List of variances for x, y, w, h.
        step: Explicit step size.

    # Input shape
        4D tensor with shape:
        `(samples, channels, rows, cols)` if dim_ordering='th'
        or 4D tensor with shape:
        `(samples, rows, cols, channels)` if dim_ordering='tf'.

    # Output shape
        3D tensor with shape:
        (samples, num_boxes, 8)

    # References
        https://arxiv.org/abs/1512.02325

    #TODO
        Add possibility not to have variances.
        Add Theano support
    """
    def __init__(self, img_size, min_size, max_size=None, aspect_ratios=None,
                 flip=True, clip=False, variances=[0.1, 0.1, 0.2, 0.2],
                 step=None, offset=0.5, **kwargs):
        if K.image_dim_ordering() == 'tf':
            self.waxis = 2
            self.haxis = 1
        else:
            self.waxis = 3
            self.haxis = 2
        self.img_size = img_size
        if min_size <= 0:
            raise Exception('min_size must be positive.')
        self.min_size = min_size
        self.max_size = max_size
        self.aspect_ratios = [1.0]
        if max_size:
            if max_size < min_size:
                raise Exception('max_size must be greater than min_size.')
            self.aspect_ratios.append(1.0)
        if aspect_ratios:
            for ar in aspect_ratios:
                if ar in self.aspect_ratios:
                    continue
                self.aspect_ratios.append(ar)
                if flip:
                    self.aspect_ratios.append(1.0 / ar)
        self.clip = clip
        self.variances = np.array(variances)
        self.step = step
        self.offset = 0.5
        super(PriorBox, self).__init__(**kwargs)

    def compute_output_shape(self, input_shape):
        num_priors_ = len(self.aspect_ratios)
        layer_width = input_shape[self.waxis]
        layer_height = input_shape[self.haxis]
        num_boxes = num_priors_ * layer_width * layer_height
        return (input_shape[0], num_boxes, 8)

    def call(self, x, mask=None):
        if hasattr(x, '_keras_shape'):
            input_shape = x._keras_shape
        elif hasattr(K, 'int_shape'):
            input_shape = K.int_shape(x)
        layer_width = input_shape[self.waxis]
        layer_height = input_shape[self.haxis]
        img_width = self.img_size[0]
        img_height = self.img_size[1]
        # define prior boxes shapes
        box_widths = []
        box_heights = []
        for ar in self.aspect_ratios:
            if ar == 1 and len(box_widths) == 0:
                box_widths.append(self.min_size)
                box_heights.append(self.min_size)
            elif ar == 1 and len(box_widths) > 0:
                box_widths.append(np.sqrt(self.min_size * self.max_size))
                box_heights.append(np.sqrt(self.min_size * self.max_size))
            elif ar != 1:
                box_widths.append(self.min_size * np.sqrt(ar))
                box_heights.append(self.min_size / np.sqrt(ar))
        box_widths = 0.5 * np.array(box_widths)
        box_heights = 0.5 * np.array(box_heights)
        # define centers of prior boxes
        if not self.step:
            step_x = img_width / layer_width
            step_y = img_height / layer_height
        else:
            step_x = self.step
            step_y = self.step
        # linx = np.linspace(0.5 * step_x, img_width - 0.5 * step_x,
        #                    layer_width)
        # liny = np.linspace(0.5 * step_y, img_height - 0.5 * step_y,
        #                    layer_height)
        linx = (np.arange(layer_height) + self.offset) * step_x
        liny = (np.arange(layer_width) + self.offset) * step_y
        centers_x, centers_y = np.meshgrid(linx, liny)
        centers_x = centers_x.reshape(-1, 1)
        centers_y = centers_y.reshape(-1, 1)
        # define xmin, ymin, xmax, ymax of prior boxes
        num_priors_ = len(self.aspect_ratios)
        prior_boxes = np.concatenate((centers_x, centers_y), axis=1)
        prior_boxes = np.tile(prior_boxes, (1, 2 * num_priors_))
        prior_boxes[:, ::4] -= box_widths
        prior_boxes[:, 1::4] -= box_heights
        prior_boxes[:, 2::4] += box_widths
        prior_boxes[:, 3::4] += box_heights
        prior_boxes[:, ::2] /= img_width
        prior_boxes[:, 1::2] /= img_height
        prior_boxes = prior_boxes.reshape(-1, 4)
        if self.clip:
            prior_boxes = np.clip(prior_boxes, 0.0, 1.0)
        # define variances
        num_boxes = len(prior_boxes)
        if len(self.variances) == 1:
            variances = np.ones((num_boxes, 4)) * self.variances[0]
        elif len(self.variances) == 4:
            variances = np.tile(self.variances, (num_boxes, 1))
        else:
            raise Exception('Must provide one or four variances.')
        prior_boxes = np.concatenate((prior_boxes, variances), axis=1)
        prior_boxes_tensor = K.expand_dims(K.variable(prior_boxes), 0)
        if K.backend() == 'tensorflow':
            pattern = [tf.shape(x)[0], 1, 1]
            prior_boxes_tensor = tf.tile(prior_boxes_tensor, pattern)
        elif K.backend() == 'theano':
            # TODO
            pass
        return prior_boxes_tensor


class SpatialBatchNormalization(Layer):
    """ TMP
    """

    def __init__(self, axis=-1, epsilon=1e-3, momentum=0.99,
                 weights=None, beta_init='zero', gamma_init='one',
                 gamma_regularizer=None, beta_regularizer=None, **kwargs):
        self.supports_masking = True
        self.axis = axis
        self.beta_init = initializers.get(beta_init)
        self.gamma_init = initializers.get(gamma_init)
        self.epsilon = epsilon
        self.momentum = momentum
        self.gamma_regularizer = regularizers.get(gamma_regularizer)
        self.beta_regularizer = regularizers.get(beta_regularizer)
        self.initial_weights = weights
        self.uses_learning_phase = True
        super(SpatialBatchNormalization, self).__init__(**kwargs)

    def build(self, input_shape):
        self.input_spec = [InputSpec(shape=input_shape)]
        shape = [1] + [d for d in input_shape[1:]]
        self.gamma = self.add_weight(shape, initializer=self.gamma_init,
                                     regularizer=self.gamma_regularizer,
                                     name='{}_gamma'.format(self.name))
        self.beta = self.add_weight(shape, initializer=self.beta_init,
                                    regularizer=self.beta_regularizer,
                                    name='{}_beta'.format(self.name))
        self.run_mean = self.add_weight(shape, initializer='zero',
                                        name='{}_run_mean'.format(self.name),
                                        trainable=False)
        self.run_std = self.add_weight(shape, initializer='one',
                                       name='{}_run_std'.format(self.name),
                                       trainable=False)
        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True

    def call(self, x, mask=None):
        assert self.built, 'Layer must be built before being called'
        mean, std = tf.nn.moments(x, [0], keep_dims=True)
        x_normed = tf.nn.batch_normalization(x, mean, std,
                                             self.beta, self.gamma,
                                             self.epsilon)

        self.add_update([assign_moving_average(self.run_mean, mean,
                                               self.momentum,
                                               zero_debias=False),
                         assign_moving_average(self.run_std, std,
                                               self.momentum,
                                               zero_debias=False)], x)
        x_normed_running = tf.nn.batch_normalization(x, self.run_mean,
                                                     self.run_std,
                                                     self.beta, self.gamma,
                                                     self.epsilon)
        # pick the normalized form of x corresponding to the training phase
        x_normed = K.in_train_phase(x_normed, x_normed_running)
        return x_normed

    def get_config(self):
        config = {'epsilon': self.epsilon,
                  'momentum': self.momentum}
        config['gamma_regularizer'] = (self.gamma_regularizer.get_config()
                                       if self.gamma_regularizer else None)
        config['beta_regularizer'] = (self.beta_regularizer.get_config()
                                      if self.beta_regularizer else None)
        base_config = super(SpatialBatchNormalization, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class NonMaximumSuppressionLayer(Layer):
    """Perform non-maximum suppression in a greedy manner.

    # Arguments
        nms_th: Nms threshold.
        top_k: Number of total bboxes to be kept per image after nms step.
        conf_th: Only consider detections,
                whose confidences are larger than a threshold.

    # Input shape
        `(samples, rows, cols, channels)

    # Output shape
        3D tensor with shape:
        (samples, num_boxes, 4 + num_classes)
    """
    def __init__(self, nms_th=0.45, top_k=400, conf_th=0.6,
                 **kwargs):
        self.nms_th = nms_th
        self.top_k = top_k
        self.conf_th = conf_th
        super(NonMaximumSuppressionLayer, self).__init__(**kwargs)

    def compute_output_shape(self, input_shape):
        num_classes = input_shape[2] - 8
        return (input_shape[0], input_shape[1], num_classes)

    def call(self, x, mask=None):
        if hasattr(x, '_keras_shape'):
            input_shape = x._keras_shape
        elif hasattr(K, 'int_shape'):
            input_shape = K.int_shape(x)
        results = []
        for pred in tf.unstack(x, num=input_shape[0]):
            mbox_loc = pred[:, :4]
            variances = pred[:, -4:]
            mbox_priorbox = pred[:, -8:-4]
            mbox_conf = pred[:, 5:-8]
            decode_bbox = self.decode_boxes(mbox_loc, mbox_priorbox, variances)
            idx_bool = tf.zeros(input_shape[1])
            for c in range(input_shape[2] - 13):
                c_confs = mbox_conf[:, c]
                c_confs_m = tf.greater(c_confs, self.conf_th)

                def idx_n_func():
                    c_confs_m_idx = tf.squeeze(tf.where(c_confs_m), axis=1)
                    c_confs_m_idx = tf.to_int32(c_confs_m_idx)
                    boxes_to_process = tf.boolean_mask(decode_bbox, c_confs_m)
                    confs_to_process = tf.boolean_mask(c_confs, c_confs_m)
                    idx = tf.image.non_max_suppression(boxes_to_process,
                                                       confs_to_process,
                                                       self.top_k, self.nms_th)
                    real_idx = tf.gather(c_confs_m_idx, idx)
                    idx_n = tf.reduce_max(tf.one_hot(real_idx, input_shape[1],
                                                     axis=0), axis=-1)
                    return idx_n

                idx_bool += tf.cond(tf.reduce_any(c_confs_m), idx_n_func,
                                    lambda: tf.zeros_like(idx_bool),
                                    name='nms_condition')
            result = tf.concat([decode_bbox, mbox_conf, idx_bool[:, None]],
                               axis=-1)
            results.append(result)
        return tf.parallel_stack(results, name='final_stack')

    def decode_boxes(self, mbox_loc, mbox_priorbox, variances):
        """Convert bboxes from local predictions to shifted priors.

        # Arguments
            mbox_loc: Tensor of predicted locations.
            mbox_priorbox: Tensor of prior boxes.
            variances: Tensor of variances.

        # Return
            decode_bbox: Shifted priors.
        """
        prior_width = mbox_priorbox[:, 2] - mbox_priorbox[:, 0]
        prior_height = mbox_priorbox[:, 3] - mbox_priorbox[:, 1]
        prior_center_x = 0.5 * (mbox_priorbox[:, 2] + mbox_priorbox[:, 0])
        prior_center_y = 0.5 * (mbox_priorbox[:, 3] + mbox_priorbox[:, 1])
        decode_bbox_center_x = mbox_loc[:, 0] * prior_width * variances[:, 0]
        decode_bbox_center_x += prior_center_x
        decode_bbox_center_y = mbox_loc[:, 1] * prior_width * variances[:, 1]
        decode_bbox_center_y += prior_center_y
        decode_bbox_width = tf.exp(mbox_loc[:, 2] * variances[:, 2])
        decode_bbox_width *= prior_width
        decode_bbox_height = tf.exp(mbox_loc[:, 3] * variances[:, 3])
        decode_bbox_height *= prior_height
        decode_bbox_xmin = decode_bbox_center_x - 0.5 * decode_bbox_width
        decode_bbox_ymin = decode_bbox_center_y - 0.5 * decode_bbox_height
        decode_bbox_xmax = decode_bbox_center_x + 0.5 * decode_bbox_width
        decode_bbox_ymax = decode_bbox_center_y + 0.5 * decode_bbox_height
        decode_bbox = tf.concat([decode_bbox_xmin[:, None],
                                 decode_bbox_ymin[:, None],
                                 decode_bbox_xmax[:, None],
                                 decode_bbox_ymax[:, None]], axis=-1)
        return tf.clip_by_value(decode_bbox, 0.0, 1.0)

    def get_config(self):
        config = {'nms_thresh': self.nms_thresh,
                  'top_k': self.top_k,
                  'confidence_threshold': self.confidence_threshold}
        base_config = super(NonMaximumSuppressionLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
