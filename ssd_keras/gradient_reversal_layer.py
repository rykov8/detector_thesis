"""Keras with Tensorflow backend implementstion of gradient reversal layer."""

import keras.backend as K
from keras.callbacks import Callback
from keras.engine.topology import Layer
import numpy as np
import tensorflow as tf
from tensorflow.python.framework import ops


def reverse_gradient(x, hp_lambda):
    """Flips the sign of the incoming gradient during training.

    # Arguments
        x: Input tensor.
        hp_lambda: Gradient reversal weight.

    # Return
        y: Indentity op applied to x with inversed gradient.

    # References
        https://arxiv.org/abs/1505.07818
    """
    try:
        reverse_gradient.num_calls += 1
    except AttributeError:
        reverse_gradient.num_calls = 1
    grad_name = "GradientReversal%d" % reverse_gradient.num_calls

    @ops.RegisterGradient(grad_name)
    def _flip_gradients(op, grad):
        return [tf.negative(grad) * hp_lambda]
    g = K.get_session().graph
    with g.gradient_override_map({'Identity': grad_name}):
        y = tf.identity(x)
    return y


class GradientReversalLayer(Layer):
    """Gradient reversal layer.

    # Arguments
        hp_lambda: Gradient reversal weight.

    # Input shape
        4D tensor with shape:
        `(samples, channels, rows, cols)` if dim_ordering='th'
        or 4D tensor with shape:
        `(samples, rows, cols, channels)` if dim_ordering='tf'.

    # Output shape
        Same as input

    # References
        https://arxiv.org/abs/1505.07818
    """
    def __init__(self, hp_lambda, **kwargs):
        super(GradientReversalLayer, self).__init__(**kwargs)
        self.hp_lambda = K.variable(hp_lambda)
        self.reverse_gradient = reverse_gradient

    def call(self, x, mask=None):
        return self.reverse_gradient(x, self.hp_lambda)

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = {"name": self.__class__.__name__,
                  "lambda": self.hp_lambda}
        base_config = super(GradientReversalLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class GRWeightScheduler(Callback):
    """Learning rate scheduler.
    # Arguments
        schedule: a function that takes an epoch index as input
            (integer, indexed from 0) and returns a new
            gradient reversal weight as output (float).
        gr_layers: List of names of gradient reversal layers.
    """
    def __init__(self, schedule, gr_layers=None):
        super(GRWeightScheduler, self).__init__()
        self.schedule = schedule
        if gr_layers is not None:
            if not isinstance(gr_layers, list):
                raise ValueError('The gr_layers parameter must be a list.')
        self.gr_layers = gr_layers

    def on_epoch_begin(self, epoch, logs={}):
        if self.gr_layers is not None:
            hp_lambda = self.schedule(epoch)
            if not isinstance(hp_lambda, (float, np.float32, np.float64)):
                raise ValueError('The output of the "schedule" function '
                                 'should be float.')
            for l_name in self.gr_layers:
                try:
                    K.set_value(self.model.get_layer(l_name).hp_lambda,
                                hp_lambda)
                except:
                    continue


class IdentityLayer(Layer):
    """Identity layer to mimic gradient reversal layer.

    # Arguments
        hp_lambda: Gradient reversal weight.

    # Input shape
        4D tensor with shape:
        `(samples, channels, rows, cols)` if dim_ordering='th'
        or 4D tensor with shape:
        `(samples, rows, cols, channels)` if dim_ordering='tf'.

    # Output shape
        Same as input

    # References
        https://arxiv.org/abs/1505.07818
    """
    def __init__(self, hp_lambda, **kwargs):
        super(IdentityLayer, self).__init__(**kwargs)

    def call(self, x, mask=None):
        return x

    def get_output_shape_for(self, input_shape):
        return input_shape
