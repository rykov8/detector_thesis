"""MAP metrics callback."""

from keras.applications.imagenet_utils import preprocess_input
from keras.callbacks import Callback
import logging
import numpy as np
from sklearn.metrics import average_precision_score
from scipy.misc import imread
from scipy.misc import imresize


class MAPCallback(Callback):
    def __init__(self, validation_data, batch_size, path_prefix,
                 gt, bbox_util, name, image_size=(300, 300),
                 map_iou_threshold=0.5, dann=True, segment=False):
        super(MAPCallback, self).__init__()
        self.validation_data = validation_data
        self.nb_val_samples = len(validation_data)
        self.batch_size = batch_size
        self.path_prefix = path_prefix
        self.gt = gt
        self.image_size = image_size
        self.bbox_util = bbox_util
        self.map_iou_threshold = map_iou_threshold
        self.dann = dann
        self.segment = segment
        self.maps = []
        self.name = name
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        open(self.name + '/log.txt', 'w')
        fh = logging.FileHandler(self.name + '/log.txt')
        fh.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(message)s')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    def _generator(self):
        for i in np.arange(0, self.nb_val_samples - self.batch_size + 1,
                           self.batch_size):
            inputs = []
            targets = []
            for j in range(self.batch_size):
                key = self.validation_data[i + j]
                img_path = self.path_prefix + key
                img = imread(img_path)
                img = imresize(img, self.image_size).astype('float32')
                inputs.append(img)
                targets.append(self.gt[key])
            yield preprocess_input(np.array(inputs)), targets

    def iou(self, box, others):
        # compute intersection
        inter_upleft = np.maximum(others[:, :2], box[:2])
        inter_botright = np.minimum(others[:, 2:4], box[2:])
        inter_wh = inter_botright - inter_upleft
        inter_wh = np.maximum(inter_wh, 0)
        inter = inter_wh[:, 0] * inter_wh[:, 1]
        # compute union
        area_pred = (box[2] - box[0]) * (box[3] - box[1])
        area_gt = (others[:, 2] - others[:, 0])
        area_gt *= (others[:, 3] - others[:, 1])
        union = area_pred + area_gt - inter
        # compute iou
        iou = inter / union
        return iou

    def eval_map(self, y_true, y_pred, map_iou_threshold):
        nb_imgs = len(y_true)
        targets = []
        preds = []
        for i in range(nb_imgs):
            true_bbs = np.array(y_true[i])
            pred_bbs = np.array(y_pred[i])
            if len(true_bbs) == 0 and len(pred_bbs) == 0:
                continue
            # assignment
            for pred_bb in pred_bbs:
                iou = self.iou(pred_bb[:4], true_bbs[:, :4])
                true_label = np.zeros_like(pred_bb[4:])
                if len(iou) == 0 or iou.max() < map_iou_threshold:
                    true_label[0] = 1
                else:
                    idx = iou.argmax()
                    true_label[1:] = true_bbs[idx, 4:]
                targets.append(true_label)
                preds.append(pred_bb[4:])
            for true_bb in true_bbs:
                iou = self.iou(true_bb[:4], pred_bbs[:, :4])
                if len(iou) == 0 or iou.max() < map_iou_threshold:
                    true_label = np.zeros(len(true_bb) - 3)
                    true_label[1:] = true_bb[4:]
                    pred_label = np.zeros_like(true_label)
                    pred_label[0] = 1
                    targets.append(true_label)
                    preds.append(pred_label)
        targets = np.array(targets)
        score = average_precision_score(targets, preds, average=None)
        return score

    def on_epoch_end(self, epoch, logs={}):
        y_true = []
        y_pred = []
        for inputs, targets in self._generator():
            preds = self.model.predict_on_batch(inputs)
            if self.dann or self.segment:
                results = self.bbox_util.detection_out(preds[0])
            else:
                results = self.bbox_util.detection_out(preds)
            y_true.extend(targets)
            y_pred.extend(results)
        score = self.eval_map(y_true, y_pred, self.map_iou_threshold)
        log_str = 'MAP for epoch {} is {:.4f}, AP is {}'.format(epoch,
                                                                score[1:].mean(),
                                                                score)
        self.logger.info(log_str)
        self.maps.append(score)
