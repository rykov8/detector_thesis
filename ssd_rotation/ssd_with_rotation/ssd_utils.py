"""Some utils for SSD."""

import cv2
import numpy as np
import Polygon.Shapes as shapes


class BBoxUtility(object):
    """Utility class to do some stuff with bounding boxes and priors.

    # Arguments
        num_classes: Number of classes including background.
        priors: Priors and variances, numpy tensor of shape (num_priors, 9),
            priors[i] = [xc, yc, w, h, alpha, varxc, varyc, varw, varh].
        overlap_threshold: Threshold to assign box to a prior.

    # References
        https://arxiv.org/abs/1512.02325
    """
    def __init__(self, num_classes, priors=None, overlap_threshold=0.5):
        self.num_classes = num_classes
        self.priors = priors
        self.rect_priors = []
        for prior in priors:
            rect = shapes.Rectangle(prior[2], prior[3])
            rect.shift(prior[0] - prior[2]/2, -prior[1] - prior[3]/2)
            rect.rotate(prior[4])
            self.rect_priors.append((rect, rect.area()))
        self.num_priors = 0 if priors is None else len(priors)
        self.overlap_threshold = overlap_threshold

    def rects_iou(self, box):
        """Compute intersection over union for the box with all priors.

        # Arguments
            box: Box, numpy tensor of shape (5,).

        # Return
            iou: Intersection over union,
                numpy tensor of shape (num_priors).
        """
        # compute intersection
        box_rect = shapes.Rectangle(box[2], box[3])
        box_rect.shift(box[0] - box[2]/2, -box[1] - box[3]/2)
        box_rect.rotate(box[4])
        box_area = box_rect.area()
        iou = np.zeros(self.num_priors)
        for i, prior_rect in enumerate(self.rect_priors):
            inter_area = (box_rect & prior_rect[0]).area()
            union_area = box_area + prior_rect[1] - inter_area
            iou[i] = inter_area / union_area
        return iou

    def encode_box(self, box, return_iou=True):
        """Encode box for training, do it only for assigned priors.

        # Arguments
            box: Box, numpy tensor of shape (5,).
            return_iou: Whether to concat iou to encoded values.

        # Return
            encoded_box: Tensor with encoded box
                numpy tensor of shape (num_priors, 6 + int(return_iou)).
        """
        iou = self.rects_iou(box)
        encoded_box = np.zeros((self.num_priors, 6 + return_iou))
        assign_mask = iou > self.overlap_threshold
        if not assign_mask.any():
            assign_mask[iou.argmax()] = True
        if return_iou:
            encoded_box[:, -1][assign_mask] = iou[assign_mask]
        assigned_priors = self.priors[assign_mask]
        box_center = box[:2]
        box_wh = box[2:4]
        assigned_priors_center = assigned_priors[:, :2]
        assigned_priors_wh = assigned_priors[:, 2:4]
        # we encode variance
        encoded_box[:, :2][assign_mask] = box_center - assigned_priors_center
        encoded_box[:, :2][assign_mask] /= assigned_priors_wh
        encoded_box[:, :2][assign_mask] /= assigned_priors[:, -4:-2]
        encoded_box[:, 2:4][assign_mask] = np.log(box_wh /
                                                  assigned_priors_wh)
        encoded_box[:, 2:4][assign_mask] /= assigned_priors[:, -2:]
        # compute angle encodings
        # relative angle
        angle_change = (box[4] - assigned_priors[:, 4])
        # absolute angle
#         angle_change = box[4]
        encoded_box[:, 4][assign_mask] = np.cos(2 * angle_change)
        encoded_box[:, 5][assign_mask] = np.sin(2 * angle_change)
        return encoded_box.ravel()

    def assign_boxes(self, boxes, return_iou=False):
        """Assign boxes to priors for training.

        # Arguments
            boxes: Box, numpy tensor of shape (num_boxes, 5 + num_classes),
                num_classes without background.

        # Return
            assignment: Tensor with assigned boxes,
                numpy tensor of shape (num_boxes, 6 + num_classes + 9),
                priors in ground truth are fictitious,
                assignment[:, -9] has 1 if prior should be penalized
                    or in other words is assigned to some ground truth box,
                assignment[:, -8:] are all 0. See loss for more details.
        """
        assignment = np.zeros((self.num_priors, 6 + self.num_classes + 9))
        assignment[:, 6] = 1.0
        if len(boxes) == 0:
            return assignment
        encoded_boxes = np.apply_along_axis(self.encode_box, 1, boxes[:, :5])
        encoded_boxes = encoded_boxes.reshape(-1, self.num_priors, 7)
        best_iou = encoded_boxes[:, :, -1].max(axis=0)
        best_iou_idx = encoded_boxes[:, :, -1].argmax(axis=0)
        best_iou_mask = best_iou > 0
        best_iou_idx = best_iou_idx[best_iou_mask]
        assign_num = len(best_iou_idx)
        encoded_boxes = encoded_boxes[:, best_iou_mask, :]
        assignment[:, :6][best_iou_mask] = encoded_boxes[best_iou_idx,
                                                         np.arange(assign_num),
                                                         :-1]
        assignment[:, 6][best_iou_mask] = 0
        assignment[:, 7:-9][best_iou_mask] = boxes[best_iou_idx, 5:]
        assignment[:, -9][best_iou_mask] = 1
        if return_iou:
            return assignment, best_iou[best_iou_mask]
        return assignment

    def decode_boxes(self, mbox_loc, mbox_priorbox, variances):
        """Convert bboxes from local predictions to shifted priors.

        # Arguments
            mbox_loc: Numpy array of predicted locations.
            mbox_priorbox: Numpy array of prior boxes.
            variances: Numpy array of variances.

        # Return
            decode_bbox: Shifted priors.
        """
        prior_width = mbox_priorbox[:, 2]
        prior_height = mbox_priorbox[:, 3]
        prior_center_x = mbox_priorbox[:, 0]
        prior_center_y = mbox_priorbox[:, 1]
        prior_angle = mbox_priorbox[:, 4]
        # decode coordinates
        decode_bbox_center_x = mbox_loc[:, 0] * prior_width * variances[:, 0]
        decode_bbox_center_x += prior_center_x
        decode_bbox_center_y = mbox_loc[:, 1] * prior_height * variances[:, 1]
        decode_bbox_center_y += prior_center_y
        decode_bbox_width = np.exp(mbox_loc[:, 2] * variances[:, 2])
        decode_bbox_width *= prior_width
        decode_bbox_height = np.exp(mbox_loc[:, 3] * variances[:, 3])
        decode_bbox_height *= prior_height
        # decode angle
        decode_bbox_angle = (np.arccos(mbox_loc[:, 4]) / 2)
        decode_bbox_angle[mbox_loc[:, 5] < 0] -= np.pi
        decode_bbox_angle[mbox_loc[:, 5] < 0] *= -1
        # relative_angle
        decode_bbox_angle += prior_angle
        decode_bbox = np.concatenate((decode_bbox_center_x[:, None],
                                      decode_bbox_center_y[:, None],
                                      decode_bbox_width[:, None],
                                      decode_bbox_height[:, None],
                                      decode_bbox_angle[:, None]), axis=-1)
        return decode_bbox

    def detection_out(self, predictions, background_label_id=0, keep_top_k=200,
                      nms_thresh=0.4, confidence_threshold=0.5):
        """Do non maximum suppression (nms) on prediction results.

        # Arguments
            predictions: Numpy array of predicted values.
            num_classes: Number of classes for prediction.
            background_label_id: Label of background class.
            keep_top_k: Number of total bboxes to be kept per image
                after nms step.
            nms_thresh: Nms threshold.
            confidence_threshold: Only consider detections,
                whose confidences are larger than a threshold.

        # Return
            results: List of predictions for every picture. Each prediction is:
                [label, confidence, xmin, ymin, xmax, ymax]
        """
        mbox_loc = predictions[:, :, :6]
        variances = predictions[:, :, -4:]
        mbox_priorbox = predictions[:, :, -9:-4]
        mbox_conf = predictions[:, :, 6:-9]
        results = []
        for i in range(len(mbox_loc)):
            results.append([])
            decode_bbox = self.decode_boxes(mbox_loc[i],
                                            mbox_priorbox[i], variances[i])
            for c in range(self.num_classes):
                if c == background_label_id:
                    continue
                c_confs = mbox_conf[i, :, c]
                c_confs_m = c_confs > confidence_threshold
                if len(c_confs[c_confs_m]) > 0:
                    boxes_to_process = decode_bbox[c_confs_m]
                    confs_to_process = c_confs[c_confs_m]
                    idx = self.do_nms(boxes_to_process,
                                      confs_to_process, nms_thresh)
                    good_boxes = boxes_to_process[idx]
                    confs = mbox_conf[i, :, :][c_confs_m][idx]
                    c_pred = np.concatenate((good_boxes, confs), axis=1)
                    results[-1].extend(c_pred)
            if len(results[-1]) > 0:
                results[-1] = np.array(results[-1])
                if len(results[-1]) > keep_top_k:
                    argsort = np.argsort(results[-1][:, 5:].max(axis=1))
                    argsort = argsort[::-1]
                    results[-1] = results[-1][argsort]
                    results[-1] = results[-1][:keep_top_k]
            else:
                results[-1] = np.array(results[-1])
                results[-1] = results[-1].reshape(-1, 5 + self.num_classes)
        return results

    def rotated_iou(self, box1, box2):
        rect1 = shapes.Rectangle(box1[2], box1[3])
        rect1.shift(box1[0] - box1[2]/2, -box1[1] - box1[3]/2)
        rect1.rotate(box1[4])
        rect2 = shapes.Rectangle(box2[2], box2[3])
        rect2.shift(box2[0] - box2[2]/2, -box2[1] - box2[3]/2)
        rect2.rotate(box2[4])
        return (rect1 & rect2).area() / (rect1 | rect2).area()

    def do_nms(self, boxes, confs, nms_thresh):
        """Greedily select boxes.

        # Arguments
            boxes: Boxes coords, tensor of shape (num_boxes, 5)
            confs: Confidenses, tensor of shape (num_boxes)
            nms_thresh: Nms threshold.

        # Returns
            idx: Indexes of selected boxes, tensor of shape (num_selected)
        """
        idx = []
        c_sorted_idx = np.argsort(confs)[::-1]
        for c_idx in c_sorted_idx:
            box = boxes[c_idx]
            should_take = True
            for saved_i in idx:
                if (np.all(np.abs(box[:2] - boxes[saved_i, :2]) > 50)):
                    continue
                iou = self.rotated_iou(box, boxes[saved_i])
                if iou >= nms_thresh:
                    should_take = False
                    break
            if should_take:
                idx.append(c_idx)
        return np.array(idx)

    def draw_box_on_image(self, img, box, color=None, text=None):
        """Draw rotated box on image.

        # Arguments
            img: Image to draw on.
            box: Box to draw, numpy tensor of shape(5,) or (8,)
            color: Optional color of the box.
            text: Optional text to put in the box.

        # Return
            img: Image with box.
        """
        img_h, img_w, _ = img.shape
        x_main = box[2] * np.cos(box[4]) * img_w
        y_main = box[2] * np.sin(box[4]) * img_h
        h_new = np.linalg.norm([x_main, y_main])
        dir_mult = np.sign(y_main) if y_main != 0 else 1
        angle_new = np.arccos(x_main * dir_mult / h_new) / np.pi * 180
        x_main = box[3] * np.cos(box[4] + 0.5*np.pi) * img_w
        y_main = box[3] * np.sin(box[4] + 0.5*np.pi) * img_h
        w_new = np.linalg.norm([x_main, y_main])
        rect = ((box[0] * img_w, box[1] * img_h),
                (h_new, w_new),
                -angle_new)
        rect_p = cv2.boxPoints(rect)
        if color is None:
            color = box[5:] * 255
        for i in range(4):
            cv2.line(img, tuple(rect_p[i]),
                     tuple(rect_p[(i + 1) % 4]), color, thickness=2)
        if text:
            cv2.putText(img, text,
                        (int(rect[0][0]), int(rect[0][1])),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.3, [255, 255, 255], 1)
        return img
