"""MAP metrics callback."""

from keras.applications.imagenet_utils import preprocess_input
from keras.callbacks import Callback
import logging
import numpy as np
import Polygon.Shapes as shapes
from sklearn.metrics import average_precision_score
from scipy.misc import imread
from scipy.misc import imresize


class MAPCallback(Callback):
    def __init__(self, validation_data, batch_size, path_prefix,
                 gt, bbox_util, name, image_size=(300, 300),
                 map_iou_threshold=0.5, dann=False, segment=False, vehicle=True):
        super(MAPCallback, self).__init__()
        self.validation_data = validation_data
        self.nb_val_samples = len(validation_data)
        self.batch_size = batch_size
        self.path_prefix = path_prefix
        self.gt = gt
        self.image_size = image_size
        self.bbox_util = bbox_util
        self.map_iou_threshold = map_iou_threshold
        self.dann = dann
        self.segment = segment
        self.maps = []
        self.vehicle = vehicle        
        self.name = name
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        open(self.name + '/log.txt', 'w')
        fh = logging.FileHandler(self.name + '/log.txt')
        fh.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(message)s')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)


    def _generator(self):
        for i in np.arange(0, self.nb_val_samples - self.batch_size + 1,
                           self.batch_size):
            inputs = []
            targets = []
            for j in range(self.batch_size):
                key = self.validation_data[i + j]
                img_path = self.path_prefix + key
                img = imread(img_path)
                img = imresize(img, self.image_size).astype('float32')
                inputs.append(img)
                if self.vehicle:
                    targets.append(self.gt[key][:, :-1])
                else:
                    targets.append(self.gt[key])
            yield preprocess_input(np.array(inputs)), targets

    def iou(self, box, others):
        iou = np.zeros(len(others))
        for i, box2 in enumerate(others):
            cur_iou = self.bbox_util.rotated_iou(box, box2)
            iou[i] = cur_iou
        return iou

    def eval_map(self, y_true, y_pred, map_iou_threshold):
        nb_imgs = len(y_true)
        targets = []
        preds = []
        for i in range(nb_imgs):
            true_bbs = np.array(y_true[i])
            pred_bbs = np.array(y_pred[i])
            if len(true_bbs) == 0 and len(pred_bbs) == 0:
                continue
            # assignment
            for pred_bb in pred_bbs:
                iou = self.iou(pred_bb[:5], true_bbs[:, :5])
                true_label = np.zeros_like(pred_bb[5:])
                if len(iou) == 0 or iou.max() < map_iou_threshold:
                    true_label[0] = 1
                else:
                    idx = iou.argmax()
                    true_label[1:] = true_bbs[idx, 5:]
                targets.append(true_label)
                preds.append(pred_bb[5:])
            for true_bb in true_bbs:
                iou = self.iou(true_bb[:5], pred_bbs[:, :5])
                if len(iou) == 0 or iou.max() < map_iou_threshold:
                    true_label = np.zeros(len(true_bb) - 4)
                    true_label[1:] = true_bb[5:]
                    pred_label = np.zeros_like(true_label)
                    pred_label[0] = 1
                    targets.append(true_label)
                    preds.append(pred_label)
        targets = np.array(targets)
        preds = np.array(preds)
        return targets, preds

    def on_epoch_end(self, epoch, logs={}):
        if epoch < 3:
            return
        y_true = []
        y_pred = []
        for inputs, targets in self._generator():
            preds = self.model.predict_on_batch(inputs)            
            if self.dann or self.segment:
                preds[0][:, :, 4] = np.clip(preds[0][0, :, 4], -1, 1)
                results = self.bbox_util.detection_out(preds[0])
            else:
                preds[:, :, 4] = np.clip(preds[0, :, 4], -1, 1)
                results = self.bbox_util.detection_out(preds,
                                                       nms_thresh=0.1,
                                                       confidence_threshold=0.6)
            y_true.extend(targets)
            y_pred.extend(results)
        targets, preds = self.eval_map(y_true, y_pred, self.map_iou_threshold)
        score = average_precision_score(targets, preds, average=None)
        log_str = 'MAP for epoch {} is {:.4f}, AP is {}'.format(epoch,
                                                                score[1:].mean(),
                                                                score)
        self.logger.info(log_str)
        self.maps.append(score)
        return targets, preds