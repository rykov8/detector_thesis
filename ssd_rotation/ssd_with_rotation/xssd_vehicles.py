"""Keras implementation of xSSD."""

import keras.backend as K
from keras.layers import Activation
from keras.layers import add
from keras.layers import BatchNormalization
from keras.layers import concatenate
from keras.layers import Conv2D
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import MaxPooling2D
from keras.layers import Reshape
from keras.layers import SeparableConv2D
from keras.models import Model
from keras.regularizers import l2

from .ssd_layers import NonMaximumSuppressionLayer
from .ssd_layers import NormCosSin
from .ssd_layers import PriorBox


def xSSD300(input_shape, num_classes=21, inference=False):
    """SSD300 architecture.

    # Arguments
        input_shape: Shape of the input image,
            expected to be either (300, 300, 3) or (3, 300, 300)(not tested).
        num_classes: Number of classes including background.
        inference: Whether to include nms layer.

    # References
        https://arxiv.org/abs/1512.02325
    """
    net = {}
    l2_reg = l2(1e-5)
    if not inference:
        input_tensor = Input(shape=input_shape)
    else:
        input_tensor = Input(batch_shape=[1] + input_shape)
    img_size = (input_shape[1], input_shape[0])
    net['input'] = input_tensor
    # Block 1
    net['conv1_1'] = Conv2D(32, (3, 3), strides=(2, 2), padding='same',
                            use_bias=False,
                            kernel_regularizer=l2_reg,
                            name='block1_conv1')(net['input'])
    net['conv1_1'] = BatchNormalization(name='block1_conv1_bn')(net['conv1_1'])
    net['conv1_1'] = Activation('relu',
                                name='block1_conv1_act')(net['conv1_1'])
    net['conv1_2'] = Conv2D(64, (3, 3), padding='same', use_bias=False,
                            kernel_regularizer=l2_reg,
                            name='block1_conv2')(net['conv1_1'])
    net['conv1_2'] = BatchNormalization(name='block1_conv2_bn')(net['conv1_2'])
    net['conv1_2'] = Activation('relu',
                                name='block1_conv2_act')(net['conv1_2'])
    net['residual1'] = Conv2D(128, (1, 1), strides=(2, 2), padding='same',
                              kernel_regularizer=l2_reg,
                              use_bias=False)(net['conv1_2'])
    net['residual1'] = BatchNormalization()(net['residual1'])
    # Block 2
    net['sepconv2_1'] = SeparableConv2D(128, (3, 3), padding='same',
                                        kernel_regularizer=l2_reg,
                                        use_bias=False,
                                        name='block2_sepconv1')(net['conv1_2'])
    bn = BatchNormalization(name='block2_sepconv1_bn')
    net['sepconv2_1'] = bn(net['sepconv2_1'])
    act = Activation('relu', name='block2_sepconv2_act')
    net['sepconv2_1'] = act(net['sepconv2_1'])
    sepconv = SeparableConv2D(128, (3, 3), padding='same', use_bias=False,
                              kernel_regularizer=l2_reg,
                              name='block2_sepconv2')
    net['sepconv2_2'] = sepconv(net['sepconv2_1'])
    bn = BatchNormalization(name='block2_sepconv2_bn')
    net['sepconv2_2'] = bn(net['sepconv2_2'])
    net['pool2'] = MaxPooling2D((3, 3), strides=(2, 2), padding='same',
                                name='block2_pool')(net['sepconv2_2'])
    net['block2'] = add([net['pool2'], net['residual1']])
    net['residual2'] = Conv2D(256, (1, 1), strides=(2, 2), padding='same',
                              kernel_regularizer=l2_reg,
                              use_bias=False)(net['block2'])
    net['residual2'] = BatchNormalization()(net['residual2'])
    # Block 3
    act = Activation('relu', name='block3_sepconv1_act')
    net['sepconv3_1'] = act(net['block2'])
    sepconv = SeparableConv2D(256, (3, 3), padding='same', use_bias=False,
                              kernel_regularizer=l2_reg,
                              name='block3_sepconv1')
    net['sepconv3_1'] = sepconv(net['sepconv3_1'])
    bn = BatchNormalization(name='block3_sepconv1_bn')
    net['sepconv3_1'] = bn(net['sepconv3_1'])
    act = Activation('relu', name='block3_sepconv2_act')
    net['sepconv3_2'] = act(net['sepconv3_1'])
    sepconv = SeparableConv2D(256, (3, 3), padding='same', use_bias=False,
                              kernel_regularizer=l2_reg,
                              name='block3_sepconv2')
    net['sepconv3_2'] = sepconv(net['sepconv3_2'])
    bn = BatchNormalization(name='block3_sepconv2_bn')
    net['sepconv3_2'] = bn(net['sepconv3_2'])
    net['pool3'] = MaxPooling2D((3, 3), strides=(2, 2), padding='same',
                                name='block3_pool')(net['sepconv3_2'])
    net['block3'] = add([net['pool3'], net['residual2']])
    net['residual3'] = Conv2D(728, (1, 1), strides=(2, 2), padding='same',
                              kernel_regularizer=l2_reg,
                              use_bias=False)(net['block3'])
    net['residual3'] = BatchNormalization()(net['residual3'])
    # Block 4
    act = Activation('relu', name='block4_sepconv1_act')
    net['sepconv4_1'] = act(net['block3'])
    sepconv = SeparableConv2D(728, (3, 3), padding='same', use_bias=False,
                              kernel_regularizer=l2_reg,
                              name='block4_sepconv1')
    net['sepconv4_1'] = sepconv(net['sepconv4_1'])
    bn = BatchNormalization(name='block4_sepconv1_bn')
    net['sepconv4_1'] = bn(net['sepconv4_1'])
    act = Activation('relu', name='block4_sepconv2_act')
    net['sepconv4_2'] = act(net['sepconv4_1'])
    sepconv = SeparableConv2D(728, (3, 3), padding='same', use_bias=False,
                              kernel_regularizer=l2_reg,
                              name='block4_sepconv2')
    net['sepconv4_2'] = sepconv(net['sepconv4_2'])
    bn = BatchNormalization(name='block4_sepconv2_bn')
    net['sepconv4_2'] = bn(net['sepconv4_2'])
    # additional
    act = Activation('relu', name='block4_sepconv3_act')
    net['sepconv4_3'] = act(net['sepconv4_2'])
    sepconv = SeparableConv2D(728, (3, 3), padding='same', use_bias=False,
                              kernel_regularizer=l2_reg,
                              name='block4_sepconv3')
    net['sepconv4_3'] = sepconv(net['sepconv4_3'])
    bn = BatchNormalization(name='block4_sepconv3_bn')
    net['sepconv4_3'] = bn(net['sepconv4_3'])
    # act = Activation('relu', name='block4_sepconv4_act')
    # net['sepconv4_4'] = act(net['sepconv4_3'])
    # sepconv = SeparableConv2D(728, (3, 3), padding='same', use_bias=False,
    #                           name='block4_sepconv4')
    # net['sepconv4_4'] = sepconv(net['sepconv4_4'])
    # bn = BatchNormalization(name='block4_sepconv4_bn')
    # net['sepconv4_4'] = bn(net['sepconv4_4'])
    # as in the paper
    net['pool4'] = MaxPooling2D((3, 3), strides=(2, 2), padding='same',
                                name='block4_pool')(net['sepconv4_3'])
    net['block4'] = add([net['pool4'], net['residual3']])
    net['residual4'] = Conv2D(1024, (1, 1), padding='same',
                              kernel_regularizer=l2_reg,
                              use_bias=False)(net['block4'])
    net['residual4'] = BatchNormalization()(net['residual4'])
    # Block 5
    act = Activation('relu', name='block5_sepconv1_act')
    net['sepconv5_1'] = act(net['block4'])
    sepconv = SeparableConv2D(1024, (3, 3), padding='same', use_bias=False,
                              kernel_regularizer=l2_reg,
                              name='block5_sepconv1')
    net['sepconv5_1'] = sepconv(net['sepconv5_1'])
    bn = BatchNormalization(name='block5_sepconv1_bn')
    net['sepconv5_1'] = bn(net['sepconv5_1'])
    act = Activation('relu', name='block5_sepconv2_act')
    net['sepconv5_2'] = act(net['sepconv5_1'])
    sepconv = SeparableConv2D(1024, (3, 3), padding='same', use_bias=False,
                              kernel_regularizer=l2_reg,
                              name='block5_sepconv2')
    net['sepconv5_2'] = sepconv(net['sepconv5_2'])
    bn = BatchNormalization(name='block5_sepconv2_bn')
    net['sepconv5_2'] = bn(net['sepconv5_2'])
    # additional
    act = Activation('relu', name='block5_sepconv3_act')
    net['sepconv5_3'] = act(net['sepconv5_2'])
    sepconv = SeparableConv2D(1024, (3, 3), padding='same', use_bias=False,
                              kernel_regularizer=l2_reg,
                              name='block5_sepconv3')
    net['sepconv5_3'] = sepconv(net['sepconv5_3'])
    bn = BatchNormalization(name='block5_sepconv3_bn')
    net['sepconv5_3'] = bn(net['sepconv5_3'])

    # act = Activation('relu', name='block5_conv4_act')
    # net['conv5_4'] = act(net['sepconv5_3'])
    # sepconv = Conv2D(1024, (3, 3), dilation_rate=6, padding='same',
    #                  use_bias=False, name='block5_conv4')
    # net['conv5_4'] = sepconv(net['conv5_4'])
    # bn = BatchNormalization(name='block5_conv3_bn')
    # net['conv5_4'] = bn(net['conv5_4'])

    net['block5'] = add([net['sepconv5_3'], net['residual4']])
    # net['block5'] = add([net['conv5_4'], net['residual4']])
    # Predictions from blocks
    num_by_angle = 8
    num_loc = 6
    num_priors = num_by_angle * 3
    blocks_prefs = {'sepconv4_3': [21.0, 45.0, 8],
                    'block5': [45.0, 99.0, 16]}
    locations = []
    confidences = []
    priors = []
    for block_pref in ['sepconv4_3', 'block5']:
        x = Conv2D(num_priors * num_loc, (3, 3), padding='same',
                   kernel_regularizer=l2_reg,
                   name=block_pref + '_mbox_loc_rot')(net[block_pref])
        net[block_pref + '_mbox_loc_rot'] = x
        flatten = Flatten(name=block_pref + '_mbox_loc_flat')
        net[block_pref + '_mbox_loc_flat'] = flatten(net[block_pref +
                                                     '_mbox_loc_rot'])
        locations.append(net[block_pref + '_mbox_loc_flat'])
        name = block_pref + '_mbox_conf'
        if num_classes != 21:
            name += '_{}'.format(num_classes)
        x = Conv2D(num_priors * num_classes, (3, 3), padding='same',
                   kernel_regularizer=l2_reg,
                   name=name)(net[block_pref])
        net[block_pref + '_mbox_conf'] = x
        flatten = Flatten(name=block_pref + '_mbox_conf_flat')
        net[block_pref + '_mbox_conf_flat'] = flatten(net[block_pref +
                                                      '_mbox_conf'])
        confidences.append(net[block_pref + '_mbox_conf_flat'])
        min_size, max_size, step = blocks_prefs[block_pref]
        priorbox = PriorBox(img_size, min_size, max_size, aspect_ratios=[2, 3],
                            clip=False, step=step, num_by_angle=num_by_angle,
                            name=block_pref + '_mbox_priorbox')
        net[block_pref + '_mbox_priorbox'] = priorbox(net[block_pref])
        priors.append(net[block_pref + '_mbox_priorbox'])
    # Gather all predictions
    net['mbox_loc'] = concatenate(locations, axis=1, name='mbox_loc')
    net['mbox_conf'] = concatenate(confidences, axis=1, name='mbox_conf')
    net['mbox_priorbox'] = concatenate(priors, axis=1, name='mbox_priorbox')
    if hasattr(net['mbox_loc'], '_keras_shape'):
        num_boxes = net['mbox_loc']._keras_shape[-1] // num_loc
    elif hasattr(net['mbox_loc'], 'int_shape'):
        num_boxes = K.int_shape(net['mbox_loc'])[-1] // num_loc
    net['mbox_loc'] = Reshape((num_boxes, num_loc),
                              name='mbox_loc_final')(net['mbox_loc'])
    net['mbox_conf'] = Reshape((num_boxes, num_classes),
                               name='mbox_conf_logits')(net['mbox_conf'])
    net['mbox_conf'] = Activation('softmax',
                                  name='mbox_conf_final')(net['mbox_conf'])
    net['predictions'] = concatenate([net['mbox_loc'],
                                      net['mbox_conf'],
                                      net['mbox_priorbox']],
                                     name='predictions_0')
    net['predictions'] = NormCosSin(name='predictions')(net['predictions'])
    output = [net['predictions']]
    if inference:
        net['nms'] = NonMaximumSuppressionLayer(name='nms')(net['predictions'])
        output = net['nms']
    model = Model(net['input'], output)
    return model
