"""Keras implementation of SSD."""

import keras.backend as K
from keras.layers import Activation
from keras.layers import concatenate
from keras.layers import Conv2D
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import MaxPooling2D
from keras.layers import Reshape
from keras.layers import ZeroPadding2D
from keras.models import Model
from keras.regularizers import l2

from .ssd_layers import NonMaximumSuppressionLayer
from .ssd_layers import Normalize
from .ssd_layers import NormCosSin
from .ssd_layers import PriorBox


def SSD300(input_shape, num_classes=21, inference=False):
    """SSD300 architecture.

    # Arguments
        input_shape: Shape of the input image,
            expected to be either (300, 300, 3) or (3, 300, 300)(not tested).
        num_classes: Number of classes including background.
        inference: Whether to include nms layer.

    # References
        https://arxiv.org/abs/1512.02325
    """
    net = {}
    l2_reg = l2(5e-4)
    # Block 1
    if not inference:
        input_tensor = Input(shape=input_shape)
    else:
        input_tensor = Input(batch_shape=[1] + input_shape)
    img_size = (input_shape[1], input_shape[0])
    net['input'] = input_tensor
    net['conv1_1'] = Conv2D(64, 3, activation='relu', padding='same',
                            name='conv1_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['input'])
    net['conv1_2'] = Conv2D(64, 3, activation='relu', padding='same',
                            name='conv1_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv1_1'])
    net['pool1'] = MaxPooling2D((2, 2), strides=(2, 2), padding='same',
                                name='pool1')(net['conv1_2'])
    # Block 2
    net['conv2_1'] = Conv2D(128, 3, activation='relu', padding='same',
                            name='conv2_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['pool1'])
    net['conv2_2'] = Conv2D(128, 3, activation='relu', padding='same',
                            name='conv2_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv2_1'])
    net['pool2'] = MaxPooling2D((2, 2), strides=(2, 2), padding='same',
                                name='pool2')(net['conv2_2'])
    # Block 3
    net['conv3_1'] = Conv2D(256, 3, activation='relu', padding='same',
                            name='conv3_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['pool2'])
    net['conv3_2'] = Conv2D(256, 3, activation='relu', padding='same',
                            name='conv3_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv3_1'])
    net['conv3_3'] = Conv2D(256, 3, activation='relu', padding='same',
                            name='conv3_3',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv3_2'])
    net['pool3'] = MaxPooling2D((2, 2), strides=(2, 2), padding='same',
                                name='pool3')(net['conv3_3'])
    # Block 4
    net['conv4_1'] = Conv2D(512, 3, activation='relu', padding='same',
                            name='conv4_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['pool3'])
    net['conv4_2'] = Conv2D(512, 3, activation='relu', padding='same',
                            name='conv4_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv4_1'])
    net['conv4_3'] = Conv2D(512, 3, activation='relu', padding='same',
                            name='conv4_3',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv4_2'])
    net['pool4'] = MaxPooling2D((2, 2), strides=(2, 2), padding='same',
                                name='pool4')(net['conv4_3'])
    # Block 5
    net['conv5_1'] = Conv2D(512, 3, activation='relu', padding='same',
                            name='conv5_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['pool4'])
    net['conv5_2'] = Conv2D(512, 3, activation='relu', padding='same',
                            name='conv5_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv5_1'])
    net['conv5_3'] = Conv2D(512, 3, activation='relu', padding='same',
                            name='conv5_3',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv5_2'])
    net['pool5'] = MaxPooling2D((3, 3), strides=(1, 1), padding='same',
                                name='pool5')(net['conv5_3'])
    # FC6
    net['fc6'] = Conv2D(1024, 3, dilation_rate=6, activation='relu',
                        padding='same', name='fc6',
                        kernel_regularizer=l2_reg,
                        bias_regularizer=l2_reg)(net['pool5'])
    # x = Dropout(0.5, name='drop6')(x)
    # FC7
    net['fc7'] = Conv2D(1024, 1, activation='relu', padding='same',
                        name='fc7',
                        kernel_regularizer=l2_reg,
                        bias_regularizer=l2_reg)(net['fc6'])
    # x = Dropout(0.5, name='drop7')(x)
    # Block 6
    net['conv6_1'] = Conv2D(256, 1, activation='relu', padding='same',
                            name='conv6_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['fc7'])
    net['conv6_2'] = Conv2D(512, 3, strides=2, activation='relu',
                            padding='same', name='conv6_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv6_1'])
    # Block 7
    net['conv7_1'] = Conv2D(128, 1, activation='relu', padding='same',
                            name='conv7_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv6_2'])
    net['conv7_2'] = ZeroPadding2D()(net['conv7_1'])
    net['conv7_2'] = Conv2D(256, 3, strides=2, activation='relu',
                            padding='valid', name='conv7_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv7_2'])
    # Block 8
    net['conv8_1'] = Conv2D(128, 1, activation='relu', padding='same',
                            name='conv8_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv7_2'])
    net['conv8_2'] = Conv2D(256, 3, strides=2, activation='relu',
                            padding='same', name='conv8_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv8_1'])
    # Block 9
    net['conv9_1'] = Conv2D(128, 1, activation='relu', padding='same',
                            name='conv9_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv8_2'])
    net['conv9_2'] = Conv2D(256, 3, activation='relu', padding='valid',
                            name='conv9_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv9_1'])
    # Prediction from conv4_3
    num_by_angle = 18
    num_loc = 6
    net['conv4_3_norm'] = Normalize(20, name='conv4_3_norm')(net['conv4_3'])
    num_priors = num_by_angle * 2
    x = Conv2D(num_priors * num_loc, 3, padding='same',
               name='conv4_3_norm_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv4_3_norm'])
    net['conv4_3_norm_mbox_loc'] = x
    flatten = Flatten(name='conv4_3_norm_mbox_loc_flat')
    net['conv4_3_norm_mbox_loc_flat'] = flatten(net['conv4_3_norm_mbox_loc'])
    name = 'conv4_3_norm_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 3, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv4_3_norm'])
    net['conv4_3_norm_mbox_conf'] = x
    flatten = Flatten(name='conv4_3_norm_mbox_conf_flat')
    net['conv4_3_norm_mbox_conf_flat'] = flatten(net['conv4_3_norm_mbox_conf'])
    priorbox = PriorBox(img_size, 21.0, 45.0, aspect_ratios=[2],
                        clip=False, step=8, num_by_angle=num_by_angle,
                        name='conv4_3_norm_mbox_priorbox')
    net['conv4_3_norm_mbox_priorbox'] = priorbox(net['conv4_3_norm'])
    # Prediction from fc7
    num_priors = num_by_angle * 3
    net['fc7_mbox_loc'] = Conv2D(num_priors * num_loc, 3, padding='same',
                                 name='fc7_mbox_loc_rot',
                                 kernel_regularizer=l2_reg,
                                 bias_regularizer=l2_reg)(net['fc7'])
    flatten = Flatten(name='fc7_mbox_loc_flat')
    net['fc7_mbox_loc_flat'] = flatten(net['fc7_mbox_loc'])
    name = 'fc7_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    net['fc7_mbox_conf'] = Conv2D(num_priors * num_classes, 3, padding='same',
                                  name=name,
                                  kernel_regularizer=l2_reg,
                                  bias_regularizer=l2_reg)(net['fc7'])
    flatten = Flatten(name='fc7_mbox_conf_flat')
    net['fc7_mbox_conf_flat'] = flatten(net['fc7_mbox_conf'])
    priorbox = PriorBox(img_size, 45.0, max_size=99.0, aspect_ratios=[2, 3],
                        clip=False, step=16, num_by_angle=num_by_angle,
                        name='fc7_mbox_priorbox')
    net['fc7_mbox_priorbox'] = priorbox(net['fc7'])
    # Prediction from conv6_2
    num_priors = num_by_angle * 3
    x = Conv2D(num_priors * num_loc, 3, padding='same',
               name='conv6_2_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv6_2'])
    net['conv6_2_mbox_loc'] = x
    flatten = Flatten(name='conv6_2_mbox_loc_flat')
    net['conv6_2_mbox_loc_flat'] = flatten(net['conv6_2_mbox_loc'])
    name = 'conv6_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 3, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv6_2'])
    net['conv6_2_mbox_conf'] = x
    flatten = Flatten(name='conv6_2_mbox_conf_flat')
    net['conv6_2_mbox_conf_flat'] = flatten(net['conv6_2_mbox_conf'])
    priorbox = PriorBox(img_size, 99.0, max_size=153.0, aspect_ratios=[2, 3],
                        clip=False, step=32, num_by_angle=num_by_angle,
                        name='conv6_2_mbox_priorbox')
    net['conv6_2_mbox_priorbox'] = priorbox(net['conv6_2'])
    # Prediction from conv7_2
    num_priors = num_by_angle * 3
    x = Conv2D(num_priors * num_loc, 3, padding='same',
               name='conv7_2_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv7_2'])
    net['conv7_2_mbox_loc'] = x
    flatten = Flatten(name='conv7_2_mbox_loc_flat')
    net['conv7_2_mbox_loc_flat'] = flatten(net['conv7_2_mbox_loc'])
    name = 'conv7_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 3, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv7_2'])
    net['conv7_2_mbox_conf'] = x
    flatten = Flatten(name='conv7_2_mbox_conf_flat')
    net['conv7_2_mbox_conf_flat'] = flatten(net['conv7_2_mbox_conf'])
    priorbox = PriorBox(img_size, 153.0, max_size=207.0, aspect_ratios=[2, 3],
                        clip=False, step=64, num_by_angle=num_by_angle,
                        name='conv7_2_mbox_priorbox')
    net['conv7_2_mbox_priorbox'] = priorbox(net['conv7_2'])
    # Prediction from conv8_2
    num_priors = num_by_angle * 2
    x = Conv2D(num_priors * num_loc, 3, padding='same',
               name='conv8_2_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv8_2'])
    net['conv8_2_mbox_loc'] = x
    flatten = Flatten(name='conv8_2_mbox_loc_flat')
    net['conv8_2_mbox_loc_flat'] = flatten(net['conv8_2_mbox_loc'])
    name = 'conv8_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 3, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv8_2'])
    net['conv8_2_mbox_conf'] = x
    flatten = Flatten(name='conv8_2_mbox_conf_flat')
    net['conv8_2_mbox_conf_flat'] = flatten(net['conv8_2_mbox_conf'])
    priorbox = PriorBox(img_size, 207.0, max_size=261.0, aspect_ratios=[2],
                        clip=False, step=100, num_by_angle=num_by_angle,
                        name='conv8_2_mbox_priorbox')
    net['conv8_2_mbox_priorbox'] = priorbox(net['conv8_2'])
    # Prediction from conv9_2
    num_priors = num_by_angle * 2
    x = Conv2D(num_priors * num_loc, 1, padding='same',
               name='conv9_2_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv9_2'])
    net['conv9_2_mbox_loc'] = x
    flatten = Flatten(name='conv9_2_mbox_loc_flat')
    net['conv9_2_mbox_loc_flat'] = flatten(net['conv9_2_mbox_loc'])
    name = 'conv9_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 1, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv9_2'])
    net['conv9_2_mbox_conf'] = x
    flatten = Flatten(name='conv9_2_mbox_conf_flat')
    net['conv9_2_mbox_conf_flat'] = flatten(net['conv9_2_mbox_conf'])
    priorbox = PriorBox(img_size, 261.0, max_size=315.0, aspect_ratios=[2],
                        clip=False, step=300, num_by_angle=num_by_angle,
                        name='conv9_2_mbox_priorbox')
    net['conv9_2_mbox_priorbox'] = priorbox(net['conv9_2'])
    # Gather all predictions
    net['mbox_loc'] = concatenate([net['conv4_3_norm_mbox_loc_flat'],
                                   net['fc7_mbox_loc_flat'],
                                   net['conv6_2_mbox_loc_flat'],
                                   net['conv7_2_mbox_loc_flat'],
                                   net['conv8_2_mbox_loc_flat'],
                                   net['conv9_2_mbox_loc_flat']], axis=1,
                                  name='mbox_loc')
    net['mbox_conf'] = concatenate([net['conv4_3_norm_mbox_conf_flat'],
                                    net['fc7_mbox_conf_flat'],
                                    net['conv6_2_mbox_conf_flat'],
                                    net['conv7_2_mbox_conf_flat'],
                                    net['conv8_2_mbox_conf_flat'],
                                    net['conv9_2_mbox_conf_flat']], axis=1,
                                   name='mbox_conf')
    net['mbox_priorbox'] = concatenate([net['conv4_3_norm_mbox_priorbox'],
                                        net['fc7_mbox_priorbox'],
                                        net['conv6_2_mbox_priorbox'],
                                        net['conv7_2_mbox_priorbox'],
                                        net['conv8_2_mbox_priorbox'],
                                        net['conv9_2_mbox_priorbox']], axis=1,
                                       name='mbox_priorbox')
    if hasattr(net['mbox_loc'], '_keras_shape'):
        num_boxes = net['mbox_loc']._keras_shape[-1] // num_loc
    elif hasattr(net['mbox_loc'], 'int_shape'):
        num_boxes = K.int_shape(net['mbox_loc'])[-1] // num_loc
    net['mbox_loc'] = Reshape((num_boxes, num_loc),
                              name='mbox_loc_final')(net['mbox_loc'])
    net['mbox_conf'] = Reshape((num_boxes, num_classes),
                               name='mbox_conf_logits')(net['mbox_conf'])
    net['mbox_conf'] = Activation('softmax',
                                  name='mbox_conf_final')(net['mbox_conf'])
    net['predictions'] = concatenate([net['mbox_loc'],
                                      net['mbox_conf'],
                                      net['mbox_priorbox']],
                                     name='predictions_0')
    net['predictions'] = NormCosSin(name='predictions')(net['predictions'])
    output = [net['predictions']]
    if inference:
        net['nms'] = NonMaximumSuppressionLayer(name='nms')(net['predictions'])
        output = net['nms']
    model = Model(net['input'], output)
    return model
