"""Keras implementation of SSD with gradient reversal layer."""

import keras.backend as K
from keras.layers import Activation
from keras.layers import BatchNormalization
from keras.layers import concatenate
from keras.layers import Conv2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import GlobalAveragePooling2D
from keras.layers import Input
from keras.layers import MaxPooling2D
from keras.layers import Reshape
from keras.layers import ZeroPadding2D
from keras.models import Model
from keras.regularizers import l2

from .gradient_reversal_layer import GradientReversalLayer
# from .ssd_layers import SpatialBatchNormalization as BatchNormalization
from .ssd_layers import NonMaximumSuppressionLayer as NMSLayer
from .ssd_layers import NormCosSin
from .ssd_layers import PriorBox


def SSD300_vehicles(input_shape, num_classes=21, dann=False, inference=False):
    """SSD300 with gradient reversal layer architecture.

    # Arguments
        input_shape: Shape of the input image,
            expected to be either (300, 300, 3) or (3, 300, 300)(not tested).
        num_classes: Number of classes including background.
        dann: Whether to add adaptation classifier.
        inference: Whether to include nms layer.

    # References
        https://arxiv.org/abs/1512.02325
        https://arxiv.org/abs/1505.07818
    """
    if K.image_dim_ordering() == 'tf':
        bn_axis = 3
    else:
        bn_axis = 1
    net = {}
    l2_reg = l2(1e-5)
    # Block 1
    if not inference:
        input_tensor = Input(shape=input_shape, name='input')
    else:
        input_tensor = Input(batch_shape=[1] + input_shape)
    img_size = (input_shape[1], input_shape[0])
    net['input'] = input_tensor
    net['conv1_1'] = Conv2D(64, 3, padding='same',
                            name='conv1_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['input'])
    # net['conv1_1'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv1_1')(net['conv1_1'])
    net['conv1_1'] = Activation('relu')(net['conv1_1'])
    net['conv1_2'] = Conv2D(64, 3, padding='same',
                            name='conv1_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv1_1'])
    # net['conv1_2'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv1_2')(net['conv1_2'])
    net['conv1_2'] = Activation('relu')(net['conv1_2'])
    net['pool1'] = MaxPooling2D((2, 2), strides=(2, 2), padding='same',
                                name='pool1')(net['conv1_2'])
    # Block 2
    net['conv2_1'] = Conv2D(128, 3, padding='same',
                            name='conv2_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['pool1'])
    # net['conv2_1'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv2_1')(net['conv2_1'])
    net['conv2_1'] = Activation('relu')(net['conv2_1'])
    net['conv2_2'] = Conv2D(128, 3, padding='same',
                            name='conv2_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv2_1'])
    # net['conv2_2'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv2_2')(net['conv2_2'])
    net['conv2_2'] = Activation('relu')(net['conv2_2'])
    net['pool2'] = MaxPooling2D((2, 2), strides=(2, 2), padding='same',
                                name='pool2')(net['conv2_2'])
    # Block 3
    net['conv3_1'] = Conv2D(256, 3, padding='same',
                            name='conv3_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['pool2'])
    # net['conv3_1'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv3_1')(net['conv3_1'])
    net['conv3_1'] = Activation('relu')(net['conv3_1'])
    net['conv3_2'] = Conv2D(256, 3, padding='same',
                            name='conv3_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv3_1'])
    # net['conv3_2'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv3_2')(net['conv3_2'])
    net['conv3_2'] = Activation('relu')(net['conv3_2'])
    net['conv3_3'] = Conv2D(256, 3, padding='same',
                            name='conv3_3',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv3_2'])
    # net['conv3_3'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv3_3')(net['conv3_3'])
    net['conv3_3'] = Activation('relu')(net['conv3_3'])
    net['pool3'] = MaxPooling2D((2, 2), strides=(2, 2), padding='same',
                                name='pool3')(net['conv3_3'])
    # Block 4
    net['conv4_1'] = Conv2D(512, 3, padding='same',
                            name='conv4_1',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['pool3'])
    # net['conv4_1'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv4_1')(net['conv4_1'])
    net['conv4_1'] = Activation('relu')(net['conv4_1'])
    net['conv4_2'] = Conv2D(512, 3, padding='same',
                            name='conv4_2',
                            kernel_regularizer=l2_reg,
                            bias_regularizer=l2_reg)(net['conv4_1'])
    # net['conv4_2'] = BatchNormalization(axis=bn_axis,
    #                                     name='bn_conv4_2')(net['conv4_2'])
    net['conv4_2'] = Activation('relu')(net['conv4_2'])
    net['conv4_3'] = Conv2D(512, 3, padding='same', use_bias=True,
                            name='conv4_3',
                            kernel_regularizer=l2_reg)(net['conv4_2'])
    net['conv4_3'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv4_3')(net['conv4_3'])
    net['conv4_3'] = Activation('relu')(net['conv4_3'])
    net['pool4'] = MaxPooling2D((2, 2), strides=(2, 2), padding='same',
                                name='pool4')(net['conv4_3'])
    # Block 5
    net['conv5_1'] = Conv2D(512, 3, padding='same', use_bias=True,
                            name='conv5_1',
                            kernel_regularizer=l2_reg)(net['pool4'])
    net['conv5_1'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv5_1')(net['conv5_1'])
    net['conv5_1'] = Activation('relu')(net['conv5_1'])
    net['conv5_2'] = Conv2D(512, 3, padding='same', use_bias=True,
                            name='conv5_2',
                            kernel_regularizer=l2_reg)(net['conv5_1'])
    net['conv5_2'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv5_2')(net['conv5_2'])
    net['conv5_2'] = Activation('relu')(net['conv5_2'])
    net['conv5_3'] = Conv2D(512, 3, padding='same', use_bias=True,
                            name='conv5_3',
                            kernel_regularizer=l2_reg)(net['conv5_2'])
    net['conv5_3'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv5_3')(net['conv5_3'])
    net['conv5_3'] = Activation('relu')(net['conv5_3'])
    net['pool5'] = MaxPooling2D((3, 3), strides=(1, 1), padding='same',
                                name='pool5')(net['conv5_3'])
    # FC6
    net['fc6'] = Conv2D(1024, 3, dilation_rate=6, padding='same',
                        use_bias=True, name='fc6',
                        kernel_regularizer=l2_reg)(net['pool5'])
    net['fc6'] = BatchNormalization(axis=bn_axis,
                                    name='bn_fc6')(net['fc6'])
    net['fc6'] = Activation('relu')(net['fc6'])
    # FC7
    net['fc7'] = Conv2D(1024, 1, padding='same', use_bias=True,
                        name='fc7',
                        kernel_regularizer=l2_reg)(net['fc6'])
    net['fc7'] = BatchNormalization(axis=bn_axis,
                                    name='bn_fc7')(net['fc7'])
    net['fc7'] = Activation('relu')(net['fc7'])
    # Block 6
    net['conv6_1'] = Conv2D(256, 1, padding='same', use_bias=True,
                            name='conv6_1',
                            kernel_regularizer=l2_reg)(net['fc7'])
    net['conv6_1'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv6_1')(net['conv6_1'])
    net['conv6_1'] = Activation('relu')(net['conv6_1'])
    net['conv6_2'] = Conv2D(512, 3, padding='same', strides=2, use_bias=True,
                            name='conv6_2',
                            kernel_regularizer=l2_reg)(net['conv6_1'])
    net['conv6_2'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv6_2')(net['conv6_2'])
    net['conv6_2'] = Activation('relu')(net['conv6_2'])
    # Block 7
    net['conv7_1'] = Conv2D(128, 1, padding='same', use_bias=True,
                            name='conv7_1',
                            kernel_regularizer=l2_reg)(net['conv6_2'])
    net['conv7_1'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv7_1')(net['conv7_1'])
    net['conv7_1'] = Activation('relu')(net['conv7_1'])
    net['conv7_2'] = ZeroPadding2D()(net['conv7_1'])
    net['conv7_2'] = Conv2D(256, 3, padding='valid', strides=2, use_bias=True,
                            name='conv7_2',
                            kernel_regularizer=l2_reg)(net['conv7_2'])
    net['conv7_2'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv7_2')(net['conv7_2'])
    net['conv7_2'] = Activation('relu')(net['conv7_2'])
    # Block 8
    net['conv8_1'] = Conv2D(128, 1, padding='same', use_bias=True,
                            name='conv8_1',
                            kernel_regularizer=l2_reg)(net['conv7_2'])
    net['conv8_1'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv8_1')(net['conv8_1'])
    net['conv8_1'] = Activation('relu')(net['conv8_1'])
    net['conv8_2'] = Conv2D(256, 3, padding='same', strides=2, use_bias=True,
                            name='conv8_2',
                            kernel_regularizer=l2_reg)(net['conv8_1'])
    net['conv8_2'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv8_2')(net['conv8_2'])
    net['conv8_2'] = Activation('relu')(net['conv8_2'])
    # Block 9
    net['conv9_1'] = Conv2D(128, 1, padding='same', use_bias=True,
                            name='conv9_1',
                            kernel_regularizer=l2_reg)(net['conv8_2'])
    net['conv9_1'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv9_1')(net['conv9_1'])
    net['conv9_1'] = Activation('relu')(net['conv9_1'])
    net['conv9_2'] = Conv2D(256, 3, padding='valid', strides=2, use_bias=True,
                            name='conv9_2',
                            kernel_regularizer=l2_reg)(net['conv9_1'])
    net['conv9_2'] = BatchNormalization(axis=bn_axis,
                                        name='bn_conv9_2')(net['conv9_2'])
    net['conv9_2'] = Activation('relu')(net['conv9_2'])
    # Prediction from conv4_3
    num_by_angle = 8
    num_loc = 6
    num_priors = num_by_angle * 3
    net['conv4_3_norm'] = net['conv4_3']
    x = Conv2D(num_priors * num_loc, 3, padding='same',
               name='conv4_3_norm_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv4_3_norm'])
    net['conv4_3_norm_mbox_loc'] = x
    flatten = Flatten(name='conv4_3_norm_mbox_loc_flat')
    net['conv4_3_norm_mbox_loc_flat'] = flatten(net['conv4_3_norm_mbox_loc'])
    name = 'conv4_3_norm_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 3, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv4_3_norm'])
    net['conv4_3_norm_mbox_conf'] = x
    flatten = Flatten(name='conv4_3_norm_mbox_conf_flat')
    net['conv4_3_norm_mbox_conf_flat'] = flatten(net['conv4_3_norm_mbox_conf'])
    priorbox = PriorBox(img_size, 21.0, 45.0, aspect_ratios=[2, 3],
                        clip=False, step=8, num_by_angle=num_by_angle,
                        name='conv4_3_norm_mbox_priorbox')
    net['conv4_3_norm_mbox_priorbox'] = priorbox(net['conv4_3_norm'])
    # Adaptation branch conv4_3
    if dann:
        grl = GradientReversalLayer(1.0, name='conv4_3_grl')
        net['conv4_3_grl'] = grl(net['conv4_3'])
        conv2d = Conv2D(512, 3, padding='same', use_bias=True,
                        name='conv4_3_ad_1', kernel_regularizer=l2_reg,)
        net['conv4_3_ad_1'] = conv2d(net['conv4_3_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_conv4_3_ad_1')
        net['conv4_3_ad_1'] = bn(net['conv4_3_ad_1'])
        net['conv4_3_ad_1'] = Activation('relu')(net['conv4_3_ad_1'])
        gap = GlobalAveragePooling2D(name='conv4_3_ad_pool')
        net['conv4_3_ad_pool'] = gap(net['conv4_3_ad_1'])
    # Prediction from fc7
    num_priors = num_by_angle * 3
    net['fc7_mbox_loc'] = Conv2D(num_priors * num_loc, 3, padding='same',
                                 name='fc7_mbox_loc_rot',
                                 kernel_regularizer=l2_reg,
                                 bias_regularizer=l2_reg)(net['fc7'])
    flatten = Flatten(name='fc7_mbox_loc_flat')
    net['fc7_mbox_loc_flat'] = flatten(net['fc7_mbox_loc'])
    name = 'fc7_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    net['fc7_mbox_conf'] = Conv2D(num_priors * num_classes, 3,
                                  padding='same',
                                  name=name,
                                  kernel_regularizer=l2_reg,
                                  bias_regularizer=l2_reg)(net['fc7'])
    flatten = Flatten(name='fc7_mbox_conf_flat')
    net['fc7_mbox_conf_flat'] = flatten(net['fc7_mbox_conf'])
    priorbox = PriorBox(img_size, 45.0, max_size=99.0, aspect_ratios=[2, 3],
                        clip=False, step=16, num_by_angle=num_by_angle,
                        name='fc7_mbox_priorbox')
    net['fc7_mbox_priorbox'] = priorbox(net['fc7'])
    # Adaptation branch fc7
    if dann:
        grl = GradientReversalLayer(1.0, name='fc7_grl')
        net['fc7_grl'] = grl(net['fc7'])
        conv2d = Conv2D(1024, 3, padding='same', use_bias=True,
                        name='fc7_ad_1',
                        kernel_regularizer=l2_reg)
        net['fc7_ad_1'] = conv2d(net['fc7_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_fc7_ad_1')
        net['fc7_ad_1'] = bn(net['fc7_ad_1'])
        net['fc7_ad_1'] = Activation('relu')(net['fc7_ad_1'])
        gap = GlobalAveragePooling2D(name='fc7_ad_pool')
        net['fc7_ad_pool'] = gap(net['fc7_ad_1'])
    # Prediction from conv6_2
    num_priors = num_by_angle * 3
    x = Conv2D(num_priors * num_loc, 3, padding='same',
               name='conv6_2_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv6_2'])
    net['conv6_2_mbox_loc'] = x
    flatten = Flatten(name='conv6_2_mbox_loc_flat')
    net['conv6_2_mbox_loc_flat'] = flatten(net['conv6_2_mbox_loc'])
    name = 'conv6_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 3, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv6_2'])
    net['conv6_2_mbox_conf'] = x
    flatten = Flatten(name='conv6_2_mbox_conf_flat')
    net['conv6_2_mbox_conf_flat'] = flatten(net['conv6_2_mbox_conf'])
    priorbox = PriorBox(img_size, 99.0, max_size=153.0, aspect_ratios=[2, 3],
                        clip=False, step=32, num_by_angle=num_by_angle,
                        name='conv6_2_mbox_priorbox')
    net['conv6_2_mbox_priorbox'] = priorbox(net['conv6_2'])
    # Adaptation branch conv6_2
    if dann:
        grl = GradientReversalLayer(1.0, name='conv6_2_grl')
        net['conv6_2_grl'] = grl(net['conv6_2'])
        conv2d = Conv2D(512, 3, padding='same', use_bias=True,
                        name='conv6_2_ad_1',
                        kernel_regularizer=l2_reg)
        net['conv6_2_ad_1'] = conv2d(net['conv6_2_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_conv6_2_ad_1')
        net['conv6_2_ad_1'] = bn(net['conv6_2_ad_1'])
        net['conv6_2_ad_1'] = Activation('relu')(net['conv6_2_ad_1'])
        gap = GlobalAveragePooling2D(name='conv6_2_ad_pool')
        net['conv6_2_ad_pool'] = gap(net['conv6_2_ad_1'])
    # Prediction from conv7_2
    num_priors = num_by_angle * 3
    x = Conv2D(num_priors * num_loc, 3, padding='same',
               name='conv7_2_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv7_2'])
    net['conv7_2_mbox_loc'] = x
    flatten = Flatten(name='conv7_2_mbox_loc_flat')
    net['conv7_2_mbox_loc_flat'] = flatten(net['conv7_2_mbox_loc'])
    name = 'conv7_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 3, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv7_2'])
    net['conv7_2_mbox_conf'] = x
    flatten = Flatten(name='conv7_2_mbox_conf_flat')
    net['conv7_2_mbox_conf_flat'] = flatten(net['conv7_2_mbox_conf'])
    priorbox = PriorBox(img_size, 153.0, max_size=207.0, aspect_ratios=[2, 3],
                        clip=False, step=64, num_by_angle=num_by_angle,
                        name='conv7_2_mbox_priorbox')
    net['conv7_2_mbox_priorbox'] = priorbox(net['conv7_2'])
    # Adaptation branch conv7_2
    if dann:
        grl = GradientReversalLayer(1.0, name='conv7_2_grl')
        net['conv7_2_grl'] = grl(net['conv7_2'])
        conv2d = Conv2D(256, 3, padding='same', use_bias=True,
                        name='conv7_2_ad_1',
                        kernel_regularizer=l2_reg)
        net['conv7_2_ad_1'] = conv2d(net['conv7_2_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_conv7_2_ad_1')
        net['conv7_2_ad_1'] = bn(net['conv7_2_ad_1'])
        net['conv7_2_ad_1'] = Activation('relu')(net['conv7_2_ad_1'])
        gap = GlobalAveragePooling2D(name='conv7_2_ad_pool')
        net['conv7_2_ad_pool'] = gap(net['conv7_2_ad_1'])
    # Prediction from conv8_2
    num_priors = num_by_angle * 2
    x = Conv2D(num_priors * num_loc, 3, padding='same',
               name='conv8_2_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv8_2'])
    net['conv8_2_mbox_loc'] = x
    flatten = Flatten(name='conv8_2_mbox_loc_flat')
    net['conv8_2_mbox_loc_flat'] = flatten(net['conv8_2_mbox_loc'])
    name = 'conv8_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 3, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv8_2'])
    net['conv8_2_mbox_conf'] = x
    flatten = Flatten(name='conv8_2_mbox_conf_flat')
    net['conv8_2_mbox_conf_flat'] = flatten(net['conv8_2_mbox_conf'])
    priorbox = PriorBox(img_size, 207.0, max_size=261.0, aspect_ratios=[2],
                        clip=False, step=100, num_by_angle=num_by_angle,
                        name='conv8_2_mbox_priorbox')
    net['conv8_2_mbox_priorbox'] = priorbox(net['conv8_2'])
    # Adaptation branch conv8_2
    if dann:
        grl = GradientReversalLayer(1.0, name='conv8_2_grl')
        net['conv8_2_grl'] = grl(net['conv8_2'])
        conv2d = Conv2D(256, 3, padding='same', use_bias=True,
                        name='conv8_2_ad_1',
                        kernel_regularizer=l2_reg)
        net['conv8_2_ad_1'] = conv2d(net['conv8_2_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_conv8_2_ad_1')
        net['conv8_2_ad_1'] = bn(net['conv8_2_ad_1'])
        net['conv8_2_ad_1'] = Activation('relu')(net['conv8_2_ad_1'])
        gap = GlobalAveragePooling2D(name='conv8_2_ad_pool')
        net['conv8_2_ad_pool'] = gap(net['conv8_2_ad_1'])
    # Prediction from conv9_2
    num_priors = num_by_angle * 2
    x = Conv2D(num_priors * num_loc, 1, padding='same',
               name='conv9_2_mbox_loc_rot',
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv9_2'])
    net['conv9_2_mbox_loc'] = x
    flatten = Flatten(name='conv9_2_mbox_loc_flat')
    net['conv9_2_mbox_loc_flat'] = flatten(net['conv9_2_mbox_loc'])
    name = 'conv9_2_mbox_conf'
    if num_classes != 21:
        name += '_{}'.format(num_classes)
    x = Conv2D(num_priors * num_classes, 1, padding='same',
               name=name,
               kernel_regularizer=l2_reg,
               bias_regularizer=l2_reg)(net['conv9_2'])
    net['conv9_2_mbox_conf'] = x
    flatten = Flatten(name='conv9_2_mbox_conf_flat')
    net['conv9_2_mbox_conf_flat'] = flatten(net['conv9_2_mbox_conf'])
    priorbox = PriorBox(img_size, 261.0, max_size=315.0, aspect_ratios=[2],
                        clip=False, step=300, num_by_angle=num_by_angle,
                        name='conv9_2_mbox_priorbox')
    net['conv9_2_mbox_priorbox'] = priorbox(net['conv9_2'])
    # Adaptation branch conv9_2
    if dann:
        grl = GradientReversalLayer(1.0, name='conv9_2_grl')
        net['conv9_2_grl'] = grl(net['conv9_2'])
        conv2d = Conv2D(256, 1, padding='same', use_bias=True,
                        name='conv9_2_ad_1',
                        kernel_regularizer=l2_reg)
        net['conv9_2_ad_1'] = conv2d(net['conv9_2_grl'])
        bn = BatchNormalization(axis=bn_axis, name='bn_conv9_2_ad_1')
        net['conv9_2_ad_1'] = bn(net['conv9_2_ad_1'])
        net['conv9_2_ad_1'] = Activation('relu')(net['conv9_2_ad_1'])
        gap = GlobalAveragePooling2D(name='conv9_2_ad_pool')
        net['conv9_2_ad_pool'] = gap(net['conv9_2_ad_1'])
    # Gather all predictions
    net['mbox_loc'] = concatenate([net['conv4_3_norm_mbox_loc_flat'],
                                   net['fc7_mbox_loc_flat']], axis=1,
                                  name='mbox_loc')
    net['mbox_conf'] = concatenate([net['conv4_3_norm_mbox_conf_flat'],
                                    net['fc7_mbox_conf_flat']], axis=1,
                                   name='mbox_conf')
    net['mbox_priorbox'] = concatenate([net['conv4_3_norm_mbox_priorbox'],
                                        net['fc7_mbox_priorbox']], axis=1,
                                       name='mbox_priorbox')
    if hasattr(net['mbox_loc'], '_keras_shape'):
        num_boxes = net['mbox_loc']._keras_shape[-1] // num_loc
    elif hasattr(net['mbox_loc'], 'int_shape'):
        num_boxes = K.int_shape(net['mbox_loc'])[-1] // num_loc
    net['mbox_loc'] = Reshape((num_boxes, num_loc),
                              name='mbox_loc_final')(net['mbox_loc'])
    net['mbox_conf'] = Reshape((num_boxes, num_classes),
                               name='mbox_conf_logits')(net['mbox_conf'])
    net['mbox_conf'] = Activation('softmax',
                                  name='mbox_conf_final')(net['mbox_conf'])
    net['predictions'] = concatenate([net['mbox_loc'],
                                      net['mbox_conf'],
                                      net['mbox_priorbox']],
                                     name='predictions_0')
    net['predictions'] = NormCosSin(name='predictions')(net['predictions'])
    output = [net['predictions']]
    if inference:
        net['nms'] = NMSLayer(name='nms')(net['predictions'])
        output = [net['nms']]
    # Adaptation branch
    if dann:
        net['ad_feature_vec'] = concatenate([net['conv4_3_ad_pool'],
                                             net['fc7_ad_pool'],
                                             net['conv6_2_ad_pool'],
                                             net['conv7_2_ad_pool'],
                                             net['conv8_2_ad_pool'],
                                             net['conv9_2_ad_pool']], axis=1,
                                            name='ad_feature_vec')
        # net['ad_feature_vec'] = net['conv4_3_ad_pool']
        dense = Dense(1, name='ad_class_logits')
        net['ad_class_logits'] = dense(net['ad_feature_vec'])
        sigmoid = Activation('sigmoid', name='ad_class')
        net['ad_class'] = sigmoid(net['ad_class_logits'])
        output.append(net['ad_class'])

    model = Model(net['input'], output)
    return model
